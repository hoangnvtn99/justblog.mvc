﻿using AutoMapper;
using FA.JustBlog.Model.PostModel;
using FA.JustBlog.Model.Reponse;
using JustBlog.Entities.Entities;
using JustBlog.Repository.Infrastructure;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

namespace FA.JustBlog.Service.PostService
{
    public class PostService : IPostService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly ILogger<PostService> _logger;

        public PostService(IUnitOfWork unitOfWork, IMapper mapper, ILogger<PostService> logger)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _logger = logger;
        }

        public TableResponse<PostDetailViewModel> GetAllPost()
        {
            TableResponse<PostDetailViewModel> result = new TableResponse<PostDetailViewModel>();
            try
            {
                var listPosts = _unitOfWork.postRepository.GetAll();
                result.Data = _mapper.Map<List<PostDetailViewModel>>(listPosts);
                result.Code = StatusCodes.Status200OK;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                result.Code = StatusCodes.Status500InternalServerError;
                result.DataError = "Error when get posts";
            }
            return result;
        }

        public Reponse<PostDetailViewModel> GetDetailPost(int year, int month, string title)
        {
            Reponse<PostDetailViewModel> reponse = new Reponse<PostDetailViewModel>();
            try
            {
                var post = _unitOfWork.postRepository.FindByValue(p => p.PostedOn.Year == year && p.PostedOn.Month == month && p.UrlSlug == title);
                var postViewModel = _mapper.Map<PostDetailViewModel>(post);
                reponse.Data = postViewModel;
                reponse.Code = StatusCodes.Status200OK;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                reponse.Code = StatusCodes.Status500InternalServerError;
                reponse.DataError = "Not Found Post";
            }
            return reponse;
        }

        public TableResponse<PostDetailViewModel> GetLastestPost(int size)
        {
            var result = new TableResponse<PostDetailViewModel>();
            try
            {
                var listPostLastest = _unitOfWork.postRepository.GetLatestPost(size);
                var listPostViewModel = _mapper.Map<List<PostDetailViewModel>>(listPostLastest);
                result.Data = listPostViewModel;
                result.Code = StatusCodes.Status200OK;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                result.Code = StatusCodes.Status500InternalServerError;
                result.DataError = "List is empty";
            }
            return result;
        }

        public TableResponse<PostDetailViewModel> GetMostPost(int size)
        {
            var result = new TableResponse<PostDetailViewModel>();
            try
            {
                var listMostPost = _unitOfWork.postRepository.GetMostViewedPost(size);
                var listPostViewModel = _mapper.Map<List<PostDetailViewModel>>(listMostPost);
                result.Data = listPostViewModel;
                result.Code = StatusCodes.Status200OK;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                result.Code = StatusCodes.Status500InternalServerError;
                result.DataError = "List is empty";
            }
            return result;
        }

        public TableResponse<PostDetailViewModel> GetDeailPostByCategory(string category)
        {
            var result = new TableResponse<PostDetailViewModel>();
            try
            {
                var listDropDown = _unitOfWork.postRepository.GetPostsByCategory(category);
                var postByCategory = _mapper.Map<List<PostDetailViewModel>>(listDropDown);
                result.Data = postByCategory;
                result.Code = StatusCodes.Status200OK;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                result.Code = StatusCodes.Status500InternalServerError;
                result.DataError = "List is empty";
            }
            return result;
        }

        public TableResponse<PostDetailViewModel> GetPostByTag(string url)
        {
            var result = new TableResponse<PostDetailViewModel>();
            try
            {
                var listDropDown = _unitOfWork.postRepository.GetPostsByTag(url);
                var postByCategory = _mapper.Map<List<PostDetailViewModel>>(listDropDown);
                result.Data = postByCategory;
                result.Code = StatusCodes.Status200OK;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                result.Code = StatusCodes.Status500InternalServerError;
                result.DataError = "List is empty";
            }
            return result;
        }

        public Reponse<PostDetailViewModel> AddPost(PostDetailViewModel postDetail)
        {
            var result = new Reponse<PostDetailViewModel>();
            try
            {
                var post = _mapper.Map<Post>(postDetail);

                _unitOfWork.postRepository.Create(post);
                _unitOfWork.SaveChanges();

                _unitOfWork.postRepository.CreatePostTagMap(post.Id, postDetail.Tags);
                _unitOfWork.SaveChanges();

                result.Code = StatusCodes.Status200OK;
                result.Message = "Create Success";
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                result.Code = StatusCodes.Status500InternalServerError;
                result.DataError = "Faile to add post";
            }
            return result;
        }

        public Reponse<PostDetailViewModel> UpdatePost(PostDetailViewModel postDetail)
        {
            var result = new Reponse<PostDetailViewModel>();
            try
            {
                var post = _mapper.Map<Post>(postDetail);
                _unitOfWork.postRepository.DeletePostTagMap(post.Id);
                _unitOfWork.SaveChanges();

                _unitOfWork.postRepository.CreatePostTagMap(post.Id, postDetail.Tags);
                _unitOfWork.SaveChanges();

                _unitOfWork.postRepository.Update(post);
                _unitOfWork.SaveChanges();

                result.Code = StatusCodes.Status200OK;
                result.Message = "Update Success";
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                result.Code = StatusCodes.Status500InternalServerError;
                result.DataError = "Faile to update post";
            }
            return result;
        }

        public Reponse<PostDetailViewModel> DeletePost(int postId)
        {
            var result = new Reponse<PostDetailViewModel>();
            try
            {
                _unitOfWork.postRepository.Delete(postId);
                _unitOfWork.SaveChanges();
                result.Code = StatusCodes.Status200OK;
                result.Message = "Delete Success";
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                result.Code = StatusCodes.Status500InternalServerError;
                result.DataError = "Faile to delete post";
            }
            return result;
        }

        public Reponse<PostDetailViewModel> GetDetailPostByPostId(int postId)
        {
            var result = new Reponse<PostDetailViewModel>();
            try
            {
                var post = _unitOfWork.postRepository.FindByValue(p => p.Id == postId);
                var postModel = _mapper.Map<PostDetailViewModel>(post);
                result.Code = StatusCodes.Status200OK;
                result.Data = postModel;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                result.Code = StatusCodes.Status500InternalServerError;
                result.DataError = "Faile to get post";
            }
            return result;
        }
    }
}