﻿using FA.JustBlog.Model.PostModel;
using FA.JustBlog.Model.Reponse;

namespace FA.JustBlog.Service.PostService
{
    public interface IPostService
    {
        /// <summary>
        ///   Get All Posts
        /// </summary>
        /// <returns>Return list post</returns>
        TableResponse<PostDetailViewModel> GetAllPost();

        /// <summary>
        ///  Get Detail Post By id
        /// </summary>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <param name="title"></param>
        /// <returns>Return a post if found in posts</returns>
        Reponse<PostDetailViewModel> GetDetailPost(int year, int month, string title);

        /// <summary>
        ///  Get laster post by size
        /// </summary>
        /// <param name="size"></param>
        /// <returns>Return a list post</returns>
        public TableResponse<PostDetailViewModel> GetLastestPost(int size);

        /// <summary>
        ///  Get most post by size
        /// </summary>
        /// <param name="size"></param>
        /// <returns>Return a post</returns>
        public TableResponse<PostDetailViewModel> GetMostPost(int size);

        /// <summary>
        ///  Get post by category
        /// </summary>
        /// <param name="category"></param>
        /// <returns>Return list post</returns>
        public TableResponse<PostDetailViewModel> GetDeailPostByCategory(string category);

        /// <summary>
        ///  Get post by tag
        /// </summary>
        /// <param name="url"></param>
        /// <returns>Return list post tag</returns>
        public TableResponse<PostDetailViewModel> GetPostByTag(string url);

        /// <summary>
        ///  Create new post in database
        /// </summary>
        /// <param name="postDetail"></param>
        /// <returns>Return message success if add in database else return message failed</returns>
        public Reponse<PostDetailViewModel> AddPost(PostDetailViewModel postDetail);

        /// <summary>
        ///  Update post by postId
        /// </summary>
        /// <param name="postDetail"></param>
        /// <returns>Return message success if update in database else return message faild</returns>
        public Reponse<PostDetailViewModel> UpdatePost(PostDetailViewModel postDetail);

        /// <summary>
        ///  Delete post by postId
        /// </summary>
        /// <param name="postDetail"></param>
        /// <returns>Return message success if delete successfull else return message failer</returns>
        public Reponse<PostDetailViewModel> DeletePost(int postDetail);

        /// <summary>
        ///  Get detail post by postId
        /// </summary>
        /// <param name="postId"></param>
        /// <returns>Return a post in database</returns>
        public Reponse<PostDetailViewModel> GetDetailPostByPostId(int postId);
    }
}