﻿using FA.JustBlog.Model.Reponse;
using FA.JustBlog.Model.TagModel;

namespace FA.JustBlog.Service.TagService
{
    public interface ITagService
    {
        /// <summary>
        ///  Get popular tag
        /// </summary>
        /// <returns>Return list tag</returns>
        public TableResponse<TagDetailViewModel> GetPopularTag();

        /// <summary>
        ///  Get Tag by Post
        /// </summary>
        /// <param name="url"></param>
        /// <returns>Return list tag</returns>
        public TableResponse<TagDetailViewModel> GetTagByPost(int postId);

        /// <summary>
        ///  Get All Tag
        /// </summary>
        /// <returns>Return a list tags</returns>
        public TableResponse<TagDetailViewModel> GetAllTags();

        /// <summary>
        ///  Get detail tag by tagId
        /// </summary>
        /// <param name="tagId"></param>
        /// <returns>Return tag if found else throw exception</returns>
        public Reponse<TagDetailViewModel> GetDetailTag(int tagId);

        /// <summary>
        ///  Add a category
        /// </summary>
        /// <param name="tagModel"></param>
        /// <returns>Return message success if add in database else return message failed</returns>
        public Reponse<TagDetailViewModel> AddTag(TagDetailViewModel tagModel);

        /// <summary>
        ///  Update a category
        /// </summary>
        /// <param name="tagModel"></param>
        /// <returns>Return message success if update in database else return message faild</returns>
        public Reponse<TagDetailViewModel> UpdateTag(TagDetailViewModel tagModel);

        /// <summary>
        ///  Delete category by
        /// </summary>
        /// <param name="tagId"></param>
        /// <returns>Return message success if delete successfull else return message failer</returns>
        public Reponse<TagDetailViewModel> DeleteTag(TagDetailViewModel tagId);
    }
}