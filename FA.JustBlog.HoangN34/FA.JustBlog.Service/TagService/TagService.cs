﻿using AutoMapper;
using FA.JustBlog.Model.Reponse;
using FA.JustBlog.Model.TagModel;
using JustBlog.Entities.Entities;
using JustBlog.Repository.Infrastructure;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

namespace FA.JustBlog.Service.TagService
{
    public class TagService : ITagService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly ILogger<TagService> _logger;

        public TagService(IUnitOfWork unitOfWork, IMapper mapper, ILogger<TagService> logger)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _logger = logger;
        }

        public TableResponse<TagDetailViewModel> GetPopularTag()
        {
            var result = new TableResponse<TagDetailViewModel>();
            try
            {
                var listPopularTag = _unitOfWork.tagRepository.GetPopularTags(10);
                var tagViewModel = _mapper.Map<List<TagDetailViewModel>>(listPopularTag);
                result.Data = tagViewModel;
                result.Code = StatusCodes.Status200OK;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                result.Code = StatusCodes.Status500InternalServerError;
                result.DataError = "Not Find Tag";
            }
            return result;
        }

        public TableResponse<TagDetailViewModel> GetTagByPost(int postId)
        {
            var result = new TableResponse<TagDetailViewModel>();
            try
            {
                var listPopularTag = _unitOfWork.tagRepository.GetTagByPost(postId);
                var tagViewModel = _mapper.Map<List<TagDetailViewModel>>(listPopularTag);
                result.Data = tagViewModel;
                result.Code = StatusCodes.Status200OK;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                result.Code = StatusCodes.Status500InternalServerError;
                result.DataError = "Not File Tag";
            }
            return result;
        }

        public TableResponse<TagDetailViewModel> GetAllTags()
        {
            var result = new TableResponse<TagDetailViewModel>();
            try
            {
                var listPopularTag = _unitOfWork.tagRepository.GetAll();
                var tagViewModel = _mapper.Map<List<TagDetailViewModel>>(listPopularTag);
                result.Data = tagViewModel;
                result.Code = StatusCodes.Status200OK;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                result.Code = StatusCodes.Status500InternalServerError;
                result.DataError = "List is empty";
            }
            return result;
        }

        public Reponse<TagDetailViewModel> GetDetailTag(int tagId)
        {
            var result = new Reponse<TagDetailViewModel>();
            try
            {
                var tag = _unitOfWork.tagRepository.FindByValue(p => p.Id == tagId);
                var tagViewModel = _mapper.Map<TagDetailViewModel>(tag);
                result.Data = tagViewModel;
                result.Code = StatusCodes.Status200OK;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                result.Code = StatusCodes.Status500InternalServerError;
                result.DataError = "Faile to get tag";
            }
            return result;
        }

        public Reponse<TagDetailViewModel> AddTag(TagDetailViewModel tagModel)
        {
            var result = new Reponse<TagDetailViewModel>();
            try
            {
                var tag = _mapper.Map<Tag>(tagModel);
                _unitOfWork.tagRepository.Create(tag);
                _unitOfWork.SaveChanges();
                result.Code = StatusCodes.Status200OK;
                result.Message = "Create Success";
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                result.Code = StatusCodes.Status500InternalServerError;
                result.DataError = "Faile to add tag";
            }
            return result;
        }

        public Reponse<TagDetailViewModel> UpdateTag(TagDetailViewModel tagModel)
        {
            var result = new Reponse<TagDetailViewModel>();
            try
            {
                var tag = _mapper.Map<Tag>(tagModel);
                _unitOfWork.tagRepository.Update(tag);
                _unitOfWork.SaveChanges();
                result.Code = StatusCodes.Status200OK;
                result.Message = "Update Success";
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                result.Code = StatusCodes.Status500InternalServerError;
                result.DataError = "Faile to  update tag";
            }
            return result;
        }

        public Reponse<TagDetailViewModel> DeleteTag(TagDetailViewModel tagModel)
        {
            var result = new Reponse<TagDetailViewModel>();
            try
            {
                var tag = _mapper.Map<Tag>(tagModel);
                _unitOfWork.tagRepository.Delete(tag);
                _unitOfWork.SaveChanges();
                result.Code = StatusCodes.Status200OK;
                result.Message = "Delete Success";
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                result.Code = StatusCodes.Status500InternalServerError;
                result.DataError = "Faile to  delete tag";
            }
            return result;
        }
    }
}