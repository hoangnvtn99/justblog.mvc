﻿using FA.JustBlog.Model.Reponse;
using FA.JustBlog.Model.UserModel;

namespace FA.JustBlog.Service.UserService
{
    public interface IUserService
    {
        /// <summary>
        ///  Get user name by email
        /// </summary>
        /// <param name="email"></param>
        /// <returns>Return a user if found else return not found</returns>
        public Reponse<UserLoginViewModel> GetUserNameByEmail(string email);

        /// <summary>
        ///  Get all user
        /// </summary>
        /// <returns>Return a list user</returns>
        public TableResponse<CreateUserModelView> GetAllUser();

        /// <summary>
        ///  Get a detail user by userId
        /// </summary>
        /// <param name="userId"></param>
        /// <returns>Return a user</returns>
        public Reponse<CreateUserModelView> GetDetailUser(string userId);

        /// <summary>
        ///  Add user in database
        /// </summary>
        /// <param name="userModel"></param>
        /// <returns>Return message success if add in database else return message failed</returns>
        public Reponse<CreateUserModelView> AddUser(CreateUserModelView userModel);

        /// <summary>
        ///  Update user by userModel
        /// </summary>
        /// <param name="userModel"></param>
        /// <returns>Return message success if update in database else return message faild</returns>
        public Task<Reponse<CreateUserModelView>> UpdateUser(CreateUserModelView userModel);

        /// <summary>
        ///  Delete user by userId
        /// </summary>
        /// <param name="userId"></param>
        /// <returns>Return message success if delete successfull else return message failer</returns>
        public Reponse<CreateUserModelView> DeleteUser(string userId);
    }
}