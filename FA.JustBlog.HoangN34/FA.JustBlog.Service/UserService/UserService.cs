﻿using AutoMapper;
using FA.JustBlog.Entities.Entities;
using FA.JustBlog.Model.Reponse;
using FA.JustBlog.Model.UserModel;
using JustBlog.Repository.Infrastructure;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;

namespace FA.JustBlog.Service.UserService
{
    public class UserService : IUserService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly ILogger _logger;
        private readonly UserManager<AppUser> _manager;

        public UserService(IUnitOfWork unitOfWork, IMapper mapper, ILogger<UserService> logger, UserManager<AppUser> manager)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _logger = logger;
            _manager = manager;
        }

        public Reponse<UserLoginViewModel> GetUserNameByEmail(string email)
        {
            var result = new Reponse<UserLoginViewModel>();
            try
            {
                var userName = _unitOfWork.userRepository.FindByValue(s => s.Email == email);
                var userViewModel = _mapper.Map<UserLoginViewModel>(userName);
                result.Data = userViewModel;
                result.Code = StatusCodes.Status200OK;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                result.Code = StatusCodes.Status500InternalServerError;
                result.DataError = "Not found email in list";
            }
            return result;
        }

        public TableResponse<CreateUserModelView> GetAllUser()
        {
            var result = new TableResponse<CreateUserModelView>();
            try
            {
                var listUser = _unitOfWork.userRepository.GetAll();
                var listUserModelView = _mapper.Map<List<CreateUserModelView>>(listUser);
                result.Data = listUserModelView;
                result.Code = StatusCodes.Status200OK;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                result.Code = StatusCodes.Status500InternalServerError;
                result.DataError = "List is empty";
            }
            return result;
        }

        public Reponse<CreateUserModelView> GetDetailUser(string userId)
        {
            var result = new Reponse<CreateUserModelView>();
            try
            {
                var User = _unitOfWork.userRepository.FindByValue(u => u.Id == userId);
                var userModelView = _mapper.Map<CreateUserModelView>(User);
                result.Data = userModelView;
                result.Code = StatusCodes.Status200OK;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                result.Code = StatusCodes.Status500InternalServerError;
                result.DataError = "List is empty";
            }
            return result;
        }

        public Reponse<CreateUserModelView> AddUser(CreateUserModelView userModel)
        {
            var result = new Reponse<CreateUserModelView>();
            try
            {
                var passwordHash = new PasswordHasher<AppUser>();

                var user = _mapper.Map<AppUser>(userModel);
                user.PasswordHash = passwordHash.HashPassword(user, userModel.Password);
                user.Id = Guid.NewGuid().ToString();
                _unitOfWork.userRepository.Create(user);
                _unitOfWork.SaveChanges();
                result.Code = StatusCodes.Status200OK;
                result.Message = "Add Success";
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                result.Code = StatusCodes.Status500InternalServerError;
                result.DataError = "Add Failer";
            }
            return result;
        }

        public async Task<Reponse<CreateUserModelView>> UpdateUser(CreateUserModelView userModel)
        {
            var result = new Reponse<CreateUserModelView>();
            try
            {
                var user = _mapper.Map<AppUser>(userModel);
                var appplication = await _manager.FindByIdAsync(user.Id);

                appplication.UserName = user.UserName;
                appplication.Age = user.Age;
                appplication.AboutMe = user.AboutMe;

                _unitOfWork.SaveChanges();
                result.Code = StatusCodes.Status200OK;
                result.Message = "Update Success";
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                result.Code = StatusCodes.Status500InternalServerError;
                result.DataError = "Update failer";
            }
            return result;
        }

        public Reponse<CreateUserModelView> DeleteUser(string userId)
        {
            var result = new Reponse<CreateUserModelView>();
            try
            {
                _unitOfWork.userRepository.DeleteUserById(userId);
                _unitOfWork.SaveChanges();
                result.Code = StatusCodes.Status200OK;
                result.Message = "Delete Success";
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                result.Code = StatusCodes.Status500InternalServerError;
                result.DataError = "Delete Failer";
            }
            return result;
        }
    }
}