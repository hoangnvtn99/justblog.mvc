﻿using AutoMapper;
using FA.JustBlog.Entities.Entities;
using FA.JustBlog.Model.CategoryModel;
using FA.JustBlog.Model.CommentModel;
using FA.JustBlog.Model.PostModel;
using FA.JustBlog.Model.RoleModel;
using FA.JustBlog.Model.TagModel;
using FA.JustBlog.Model.UserModel;
using JustBlog.Entities.Entities;
using Microsoft.AspNetCore.Identity;

namespace FA.JustBlog.Service.Mapper
{
    public class AutoMapConfig : Profile
    {
        public AutoMapConfig()
        {
            CreateMap<Post, PostDetailViewModel>().ReverseMap();
            CreateMap<Category, CategoryDetailViewModel>().ReverseMap();
            CreateMap<Comment, CommentDetailViewModel>().ReverseMap();
            CreateMap<Tag, TagDetailViewModel>().ReverseMap();
            CreateMap<AppUser, UserModel>().ReverseMap();
            CreateMap<IdentityRole, RoleModel>().ReverseMap();
            CreateMap<AppUser, UserLoginViewModel>().ReverseMap();
            CreateMap<AppUser, CreateUserModelView>().ReverseMap();
            //CreateMap<IdentityUser, UserModel>().ReverseMap();
        }
    }
}