﻿using FA.JustBlog.Model.CategoryModel;
using FA.JustBlog.Model.Reponse;

namespace FA.JustBlog.Service.CategoryService
{
    public interface ICategoryService
    {
        /// <summary>
        ///  Get all categories
        /// </summary>
        /// <returns>Return list categories</returns>
        public TableResponse<CategoryDetailViewModel> GetCategoryDropDown();

        /// <summary>
        ///  Get category by post
        /// </summary>
        /// <param name="postid"></param>
        /// <returns>Return a category</returns>
        public Reponse<CategoryDetailViewModel> GetCategoryByPost(int postid);

        /// <summary>
        ///  Get detail category by id
        /// </summary>
        /// <param name="categoryId"></param>
        /// <returns>Return a category if found else return message</returns>
        public Reponse<CategoryDetailViewModel> GetDetailCategory(int categoryId);

        /// <summary>
        ///  Add a category by category model
        /// </summary>
        /// <param name="categoryModel"></param>
        /// <returns>Return message success if add in database else return message failed</returns>
        public Reponse<CategoryDetailViewModel> AddCategory(CategoryDetailViewModel categoryModel);

        /// <summary>
        ///  Update category by category
        /// </summary>
        /// <param name="categoryModel"></param>
        /// <returns>Return message success if update in database else return message faild</returns>
        public Reponse<CategoryDetailViewModel> UpdateCategory(CategoryDetailViewModel categoryModel);

        /// <summary>
        ///  Delete category by category
        /// </summary>
        /// <param name="categoryModel"></param>
        /// <returns>Return message success if delete successfull else return message failer</returns>
        public Reponse<CategoryDetailViewModel> DeleteCategory(CategoryDetailViewModel categoryModel);
    }
}