﻿using AutoMapper;
using FA.JustBlog.Model.CategoryModel;
using FA.JustBlog.Model.Reponse;
using JustBlog.Entities.Entities;
using JustBlog.Repository.Infrastructure;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

namespace FA.JustBlog.Service.CategoryService
{
    public class CategoryService : ICategoryService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly ILogger<CategoryService> _logger;

        public CategoryService(IUnitOfWork unitOfWork, IMapper mapper, ILogger<CategoryService> logger)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _logger = logger;
        }

        public TableResponse<CategoryDetailViewModel> GetCategoryDropDown()
        {
            var result = new TableResponse<CategoryDetailViewModel>();
            try
            {
                var listDropDown = _unitOfWork.categoryRepository.GetAll();
                var categoryViewModel = _mapper.Map<List<CategoryDetailViewModel>>(listDropDown);
                result.Data = categoryViewModel;
                result.Code = StatusCodes.Status200OK;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                result.Code = StatusCodes.Status500InternalServerError;
                result.DataError = "list is empty";
            }
            return result;
        }

        public Reponse<CategoryDetailViewModel> GetCategoryByPost(int postid)
        {
            var result = new Reponse<CategoryDetailViewModel>();
            try
            {
                var listDropDown = _unitOfWork.categoryRepository.GetCategoryByPost(postid);
                var categoryViewModel = _mapper.Map<CategoryDetailViewModel>(listDropDown);
                result.Data = categoryViewModel;
                result.Code = StatusCodes.Status200OK;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                result.Code = StatusCodes.Status500InternalServerError;
                result.DataError = "Category is exits in list";
            }
            return result;
        }

        public Reponse<CategoryDetailViewModel> GetDetailCategory(int categoryId)
        {
            var result = new Reponse<CategoryDetailViewModel>();
            try
            {
                var category = _unitOfWork.categoryRepository.FindByValue(p => p.Id == categoryId);
                var categoryViewModel = _mapper.Map<CategoryDetailViewModel>(category);
                result.Data = categoryViewModel;
                result.Code = StatusCodes.Status200OK;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                result.Code = StatusCodes.Status500InternalServerError;
                result.DataError = "List is empty";
            }
            return result;
        }

        public Reponse<CategoryDetailViewModel> AddCategory(CategoryDetailViewModel categoryModel)
        {
            var result = new Reponse<CategoryDetailViewModel>();
            try
            {
                var category = _mapper.Map<Category>(categoryModel);
                _unitOfWork.categoryRepository.Create(category);
                _unitOfWork.SaveChanges();
                result.Code = StatusCodes.Status200OK;
                result.DataError = "Create Success";
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                result.Code = StatusCodes.Status500InternalServerError;
                result.DataError = "Faile to add category";
            }
            return result;
        }

        public Reponse<CategoryDetailViewModel> UpdateCategory(CategoryDetailViewModel categoryModel)
        {
            var result = new Reponse<CategoryDetailViewModel>();
            try
            {
                var category = _mapper.Map<Category>(categoryModel);
                _unitOfWork.categoryRepository.Update(category);
                _unitOfWork.SaveChanges();
                result.Code = StatusCodes.Status200OK;
                result.Message = "Update Success";
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                result.Code = StatusCodes.Status500InternalServerError;
                result.DataError = "Faile to update category";
            }
            return result;
        }

        public Reponse<CategoryDetailViewModel> DeleteCategory(CategoryDetailViewModel categoryModel)
        {
            var result = new Reponse<CategoryDetailViewModel>();
            try
            {
                var category = _mapper.Map<Category>(categoryModel);
                _unitOfWork.categoryRepository.Delete(category);
                _unitOfWork.SaveChanges();
                result.Code = StatusCodes.Status200OK;
                result.Message = "Delete Success";
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                result.Code = StatusCodes.Status500InternalServerError;
                result.DataError = "Faile to delete category";
            }
            return result;
        }
    }
}