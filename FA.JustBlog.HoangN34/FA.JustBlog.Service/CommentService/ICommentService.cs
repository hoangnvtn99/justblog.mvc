﻿using FA.JustBlog.Model.CommentModel;
using FA.JustBlog.Model.Reponse;

namespace FA.JustBlog.Service.CommentService
{
    public interface ICommentService
    {
        /// <summary>
        ///  Add comment to post by id
        /// </summary>
        /// <param name="commentModel"></param>
        /// <param name="postId"></param>
        /// <returns>Return message if create success else return fail message</returns>
        public Reponse<string> CreateComment(CommentDetailViewModel commentModel, int postId);

        /// <summary>
        ///  Get comment by postId
        /// </summary>
        /// <param name="postId"></param>
        /// <returns>Return a list comment</returns>
        public TableResponse<CommentDetailViewModel> GetCommentByPost(int postId);

        /// <summary>
        ///  Get all comments
        /// </summary>
        /// <returns>Return list comments</returns>
        public TableResponse<CommentDetailViewModel> GetAllComments();

        /// <summary>
        ///  Get detail comment by commentId
        /// </summary>
        /// <param name="commentId"></param>
        /// <returns>Return a category if found else return message</returns>
        public Reponse<CommentDetailViewModel> GetDetailComment(int commentId);

        /// <summary>
        ///  Update comment by commentModel
        /// </summary>
        /// <param name="commentModel"></param>
        /// <returns>Return message success if update in database else return message faild</returns>
        public Reponse<CommentDetailViewModel> UpdateComment(CommentDetailViewModel commentModel);

        /// <summary>
        ///  Delete comment by commentModel
        /// </summary>
        /// <param name="commentModel"></param>
        /// <returns>Return message success if delete successfull else return message failer</returns>
        public Reponse<CommentDetailViewModel> DeleteComment(CommentDetailViewModel commentModel);
    }
}