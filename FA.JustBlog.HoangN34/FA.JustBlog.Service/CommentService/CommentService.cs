﻿using AutoMapper;
using FA.JustBlog.Model.CommentModel;
using FA.JustBlog.Model.Reponse;
using JustBlog.Entities.Entities;
using JustBlog.Repository.Infrastructure;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

namespace FA.JustBlog.Service.CommentService
{
    public class CommentService : ICommentService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly ILogger<CommentService> _logger;

        public CommentService(IUnitOfWork unitOfWork, IMapper mapper, ILogger<CommentService> logger)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _logger = logger;
        }

        public TableResponse<CommentDetailViewModel> GetCommentByPost(int postId)
        {
            var result = new TableResponse<CommentDetailViewModel>();
            try
            {
                var listComment = _unitOfWork.commentRepository.GetCommentsForPost(postId);
                var commentsModel = _mapper.Map<List<CommentDetailViewModel>>(listComment);
                result.Data = commentsModel;
                result.Code = StatusCodes.Status200OK;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                result.Code = StatusCodes.Status500InternalServerError;
                result.DataError = "List is  Empty";
            }
            return result;
        }

        public TableResponse<CommentDetailViewModel> GetAllComments()
        {
            var result = new TableResponse<CommentDetailViewModel>();
            try
            {
                var listComment = _unitOfWork.commentRepository.GetAll();
                var commentsModel = _mapper.Map<List<CommentDetailViewModel>>(listComment);
                result.Data = commentsModel;
                result.Code = StatusCodes.Status200OK;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                result.Code = StatusCodes.Status500InternalServerError;
                result.DataError = "List Empty";
            }
            return result;
        }

        public Reponse<string> CreateComment(CommentDetailViewModel commentModel, int postId)
        {
            var result = new Reponse<string>();
            try
            {
                if (commentModel == null)
                {
                    result.Code = StatusCodes.Status400BadRequest;
                    result.DataError = "Comment is empty";
                    return result;
                }
                var comment = _mapper.Map<Comment>(commentModel);
                _unitOfWork.commentRepository.AddComment(postId, comment.CommentName, comment.Email, comment.CommentHeader, comment.CommentText);
                _unitOfWork.SaveChanges();

                result.Code = StatusCodes.Status200OK;
                result.Message = "Create Success";
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                result.Code = StatusCodes.Status500InternalServerError;
                result.DataError = "Add Failer!!!";
            }
            return result;
        }

        public Reponse<CommentDetailViewModel> GetDetailComment(int commentId)
        {
            var result = new Reponse<CommentDetailViewModel>();
            try
            {
                var comment = _unitOfWork.commentRepository.FindByValue(p => p.Id == commentId);
                var commentViewModel = _mapper.Map<CommentDetailViewModel>(comment);
                result.Data = commentViewModel;
                result.Code = StatusCodes.Status200OK;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                result.Code = StatusCodes.Status500InternalServerError;
                result.DataError = "Comment is not exits in list";
            }
            return result;
        }

        public Reponse<CommentDetailViewModel> UpdateComment(CommentDetailViewModel commentModel)
        {
            var result = new Reponse<CommentDetailViewModel>();
            try
            {
                var comment = _mapper.Map<Comment>(commentModel);
                _unitOfWork.commentRepository.Update(comment);
                _unitOfWork.SaveChanges();
                result.Code = StatusCodes.Status200OK;
                result.Message = "Update Success";
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                result.Code = StatusCodes.Status500InternalServerError;
                result.DataError = "Faile to update comment";
            }
            return result;
        }

        public Reponse<CommentDetailViewModel> DeleteComment(CommentDetailViewModel commentModel)
        {
            var result = new Reponse<CommentDetailViewModel>();
            try
            {
                var comment = _mapper.Map<Comment>(commentModel);
                _unitOfWork.commentRepository.Delete(comment);
                _unitOfWork.SaveChanges();
                result.Code = StatusCodes.Status200OK;
                result.Message = "Delete Success";
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                result.Code = StatusCodes.Status500InternalServerError;
                result.DataError = "Faile to add comment";
            }
            return result;
        }
    }
}