﻿using FA.JustBlog.Model.Reponse;
using FA.JustBlog.Model.RoleModel;

namespace FA.JustBlog.Service.RoleService
{
    public interface IRoleService
    {
        /// <summary>
        ///  Get all roles
        /// </summary>
        /// <returns>Return a list role</returns>
        public TableResponse<RoleModel> GetAllRole();

        /// <summary>
        ///  Get detail role by roleId
        /// </summary>
        /// <param name="roleId"></param>
        /// <returns>Return a role </returns>
        public Reponse<RoleModel> GetDetailRole(string roleId);

        /// <summary>
        ///  Add role by roleModel
        /// </summary>
        /// <param name="roleModel"></param>
        /// <returns>Return message success if add in database else return message failed</returns>
        public Reponse<RoleModel> AddRole(RoleModel roleModel);

        /// <summary>
        ///  Update role by role model
        /// </summary>
        /// <param name="roleModel"></param>
        /// <returns>Return message success if update in database else return message faild</returns>
        public Reponse<RoleModel> UpdateRole(RoleModel roleModel);

        /// <summary>
        ///  Delete role by roleId
        /// </summary>
        /// <param name="roleId"></param>
        /// <returns>Return message success if delete successfull else return message failer</returns>
        public Reponse<RoleModel> DeleteRole(string roleId);
    }
}