﻿using AutoMapper;
using FA.JustBlog.Model.Reponse;
using FA.JustBlog.Model.RoleModel;
using JustBlog.Repository.Infrastructure;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;

namespace FA.JustBlog.Service.RoleService
{
    public class RoleService : IRoleService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly ILogger<RoleService> _logger;

        public RoleService(IUnitOfWork unitOfWork, IMapper mapper, ILogger<RoleService> logger)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _logger = logger;
        }

        public TableResponse<RoleModel> GetAllRole()
        {
            var result = new TableResponse<RoleModel>();
            try
            {
                var listRole = _unitOfWork.roleRepository.GetAll();
                var listRoleModelView = _mapper.Map<List<RoleModel>>(listRole);
                result.Data = listRoleModelView;
                result.Code = StatusCodes.Status200OK;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                result.Code = StatusCodes.Status500InternalServerError;
                result.DataError = "List is empty";
            }
            return result;
        }

        public Reponse<RoleModel> GetDetailRole(string roleId)
        {
            var result = new Reponse<RoleModel>();
            try
            {
                var role = _unitOfWork.roleRepository.FindByValue(u => u.Id == roleId);
                var roleModelView = _mapper.Map<RoleModel>(role);
                result.Data = roleModelView;
                result.Code = StatusCodes.Status200OK;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                result.Code = StatusCodes.Status500InternalServerError;
                result.DataError = "role is empty";
            }
            return result;
        }

        public Reponse<RoleModel> AddRole(RoleModel roleModel)
        {
            var result = new Reponse<RoleModel>();
            try
            {
                var role = _mapper.Map<IdentityRole>(roleModel);

                role.Id = Guid.NewGuid().ToString();
                _unitOfWork.roleRepository.Create(role);
                _unitOfWork.SaveChanges();

                result.Code = StatusCodes.Status200OK;
                result.Message = "Add Success";
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                result.Code = StatusCodes.Status500InternalServerError;
                result.DataError = "Add Failer";
            }
            return result;
        }

        public Reponse<RoleModel> UpdateRole(RoleModel roleModel)
        {
            var result = new Reponse<RoleModel>();
            try
            {
                var user = _mapper.Map<IdentityRole>(roleModel);
                _unitOfWork.roleRepository.Update(user);
                _unitOfWork.SaveChanges();

                result.Code = StatusCodes.Status200OK;
                result.Message = "Update Success";
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                result.Code = StatusCodes.Status500InternalServerError;
                result.DataError = "Update Failer";
            }
            return result;
        }

        public Reponse<RoleModel> DeleteRole(string roleId)
        {
            var result = new Reponse<RoleModel>();
            try
            {
                _unitOfWork.roleRepository.DeleleRoleById(roleId);
                _unitOfWork.SaveChanges();

                result.Code = StatusCodes.Status200OK;
                result.Message = "Delete Success";
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                result.Code = StatusCodes.Status500InternalServerError;
                result.DataError = "Delete failer";
            }
            return result;
        }
    }
}