﻿using System.ComponentModel.DataAnnotations;

namespace FA.JustBlog.Model.UserModel
{
    public class CreateUserModelView
    {
        public string? Id { get; set; }

        [Required(ErrorMessage = "Please input value {0}")]
        public string? UserName { get; set; }

        [Required(ErrorMessage = "Please input value {0}")]
        public string? NormalizedUserName { get; set; }

        [Required(ErrorMessage = "Please input value {0}")]
        [EmailAddress(ErrorMessage = "Input correct email")]
        public string? Email { get; set; }

        [Required(ErrorMessage = "Please input value {0}")]
        public string? NormalizedEmail { get; set; }

        public bool EmailConfirmed { get; set; }
        [Required(ErrorMessage = "Please input value {0}")]
        [DataType(DataType.Password, ErrorMessage = "Input correct password")]
        public virtual string? Password { get; set; }

        [Required(ErrorMessage = "Please input value {0}")]
        public int Age { get; set; }

        public string? AboutMe { get; set; }
    }
}