﻿using System.ComponentModel.DataAnnotations;

namespace FA.JustBlog.Model.UserModel
{
    public class UserModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Please input value {0}")]
        public string? UserName { get; set; }

        [Required(ErrorMessage = "Please input value {0}")]
        [EmailAddress(ErrorMessage = "Input correct email")]
        public string? Email { get; set; }

        public string? PasswordHash { get; set; }
    }
}