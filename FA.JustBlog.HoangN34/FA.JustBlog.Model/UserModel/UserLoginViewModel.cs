﻿using System.ComponentModel.DataAnnotations;

namespace FA.JustBlog.Model.UserModel
{
    public class UserLoginViewModel
    {
        [Required(ErrorMessage = "Please input {0}")]
        [EmailAddress]
        public string? Email { get; set; }

        [Required(ErrorMessage = "Please input {0}")]
        [DataType(DataType.Password)]
        public string? Password { get; set; }

        public string? UserName { get; set; }

        [Range(minimum: 1, maximum: 1, ErrorMessage = "Pleae check value")]
        public bool RememberMe { get; set; }
    }
}