﻿using System.ComponentModel.DataAnnotations;

namespace FA.JustBlog.Model.TagModel
{
    public class TagDetailViewModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Please input value {0}")]
        public string TagName { get; set; } = string.Empty;

        [Required(ErrorMessage = "Please input value {0}")]
        public string UrlSlug { get; set; } = string.Empty;

        [Required(ErrorMessage = "Please input value {0}")]
        public string Description { get; set; } = string.Empty;

        [Required(ErrorMessage = "Please input value {0}")]
        public int Count { get; set; }
    }
}