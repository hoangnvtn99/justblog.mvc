﻿using JustBlog.Entities.Entities;
using System.ComponentModel.DataAnnotations;

namespace FA.JustBlog.Model.PostModel
{
    public class PostDetailViewModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Please input value {0}")]
        public string Title { get; set; } = string.Empty;

        [Required(ErrorMessage = "Please input value {0}")]
        public string ShortDescription { get; set; } = string.Empty;

        [Required(ErrorMessage = "Please input value {0}")]
        public string PostContent { get; set; } = string.Empty;

        [Required(ErrorMessage = "Please input value {0}")]
        public string UrlSlug { get; set; } = string.Empty;

        [Required(ErrorMessage = "Please check value {0}")]
        public bool Published { get; set; }

        public DateTime PostedOn { get; set; }

        public DateTime Modified { get; set; }

        [Required(ErrorMessage = "Please input value ")]
        public int ViewCount { get; set; }

        [Required(ErrorMessage = "Please input value ")]
        public int RateCount { get; set; }

        [Required(ErrorMessage = "Please input value ")]
        public int TotalRate { get; set; }

        public decimal Rate { get; }

        public int CategoryId { get; set; }
        public Category? Category { get; set; }

        public List<int>? Tags { get; set; }
    }
}