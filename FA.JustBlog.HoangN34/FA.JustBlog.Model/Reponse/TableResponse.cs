﻿using Microsoft.AspNetCore.Http;

namespace FA.JustBlog.Model.Reponse
{
    public class TableResponse<T>
    {
        public TableResponse()
        {
            Code = StatusCodes.Status200OK;
            RecordsTotal = RecordsFiltered = 0;
            Data = new List<T>();
        }

        public int Code { get; set; }
        public Boolean Success { get; set; }
        public string Message { get; set; }
        public string DataError { get; set; }
        public int Draw { get; set; }
        public int RecordsTotal { get; set; }
        public int RecordsFiltered { get; set; }
        public List<T> Data { get; set; }
    }
}