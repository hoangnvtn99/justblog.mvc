﻿using Microsoft.AspNetCore.Http;

namespace FA.JustBlog.Model.Reponse
{
    public class Reponse<T> where T : class
    {
        public Reponse()
        {
            Code = StatusCodes.Status200OK;
        }

        public int Code { get; set; }
        public string Message { get; set; }
        public string DataError { get; set; }
        public T Data { get; set; }

        public List<T> DataList { get; set; }
    }
}