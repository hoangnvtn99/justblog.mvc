﻿using System.ComponentModel.DataAnnotations;

namespace FA.JustBlog.Model.CategoryModel
{
    public class CategoryDetailViewModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Please input value {0}")]
        public string CategoryName { get; set; } = string.Empty;

        [Required(ErrorMessage = "Please input value {0}")]
        public string UrlSlug { get; set; } = string.Empty;

        public string Description { get; set; } = string.Empty;
    }
}