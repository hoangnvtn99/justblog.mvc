﻿using System.ComponentModel.DataAnnotations;

namespace FA.JustBlog.Model.RoleModel
{
    public class RoleModel
    {
        public string? Id { get; set; }

        [Required(ErrorMessage = "Please input value {0}")]
        public string? Name { get; set; }

        [Required(ErrorMessage = "Please input value {0}")]
        public string? NormalizedName { get; set; }

        public string? ConcurrencyStamp { get; set; }
    }
}