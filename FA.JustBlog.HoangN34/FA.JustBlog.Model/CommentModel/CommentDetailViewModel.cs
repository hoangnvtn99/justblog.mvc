﻿using System.ComponentModel.DataAnnotations;

namespace FA.JustBlog.Model.CommentModel
{
    public class CommentDetailViewModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Please input value {0}")]
        public string CommentName { get; set; } = string.Empty;

        [EmailAddress(ErrorMessage = "Input Correct Email")]
        [Required(ErrorMessage = "Please input value {0}")]
        public string Email { get; set; } = string.Empty;

        [Required(ErrorMessage = "Please input value {0}")]
        public string CommentHeader { get; set; } = string.Empty;

        [Required(ErrorMessage = "Please input value {0}")]
        public string CommentText { get; set; } = string.Empty;

        public DateTime CommentTime { get; set; }
        public int PostId { get; set; }
    }
}