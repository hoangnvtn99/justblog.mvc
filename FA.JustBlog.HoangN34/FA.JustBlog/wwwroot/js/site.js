﻿$('#Description').ckeditor();

getCommentsByPost();

function getCommentsByPost() {
    let commentsBox = document.getElementById('comments-box');
    if (commentsBox != null) {
        let postId = document.getElementById('postId').value;
        fetch(`/Comment/Index/${postId}`).then(re => re.text()).then(partialView => commentsBox.innerHTML = partialView);
    }
}

function isNullOrEmpty(s) {
    if (s == undefined || s == null || s.length == '') {
        return true;
    }
    return false;
}

$('#submit-comment-btn').on('click', function () {
    let comment = {
        postId: $('#postId').val(),
        commentHeader: $('#comment-header').val(),
        commentName: $('#comment-name').val(),
        email: $('#comment-email').val(),
        commentText: $('#comment-text').val()
    }
    if (isNullOrEmpty(comment.commentHeader) || isNullOrEmpty(comment.commentText)) {
        alert('Name, Email and comment text must be not empty');
    }
    else {
        $.ajax({
            url: '/comment/AddComment',
            type: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            data: JSON.stringify({
                postId: $('#postId').val(),
                commentHeader: $('#comment-header').val(),
                commentName: $('#comment-name').val(),
                email: $('#comment-email').val(),
                commentText: $('#comment-text').val()
            })
        }).done(function () {
            $('#comment-header').val('');
            $('#comment-name').val('');
            $('#comment-email').val('');
            $('#comment-text').val('');
            getCommentsByPost();
            alert('Add comment success!');
        });
    }
});