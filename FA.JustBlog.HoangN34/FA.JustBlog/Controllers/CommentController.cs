﻿using FA.JustBlog.Model.CommentModel;
using FA.JustBlog.Service.CommentService;
using Microsoft.AspNetCore.Mvc;

namespace FA.JustBlog.Controllers
{
    public class CommentController : Controller
    {
        private readonly ICommentService _service;

        public CommentController(ICommentService service)
        {
            _service = service;
        }

        public IActionResult Index(int id)
        {
            var listComment = _service.GetCommentByPost(id).Data;
            if (listComment.Count > 0)
            {
                return PartialView("~/Views/Shared/_Comment.cshtml", listComment);
            }
            return Redirect("~Views/Shared/Error.cshtml");
        }

        [HttpPost]
        public IActionResult AddComment([FromBody] CommentDetailViewModel comment)
        {
            var listComment = _service.CreateComment(comment, comment.PostId);
            return StatusCode(listComment.Code);
        }
    }
}