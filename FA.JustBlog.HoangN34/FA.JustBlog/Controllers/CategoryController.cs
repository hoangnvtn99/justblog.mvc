﻿using FA.JustBlog.Service.CategoryService;
using FA.JustBlog.Service.PostService;
using Microsoft.AspNetCore.Mvc;

namespace FA.JustBlog.Controllers
{
    public class CategoryController : Controller
    {
        private readonly ICategoryService _service;
        private readonly IPostService _postService;

        public CategoryController(ICategoryService service, IPostService postService)
        {
            _service = service;
            _postService = postService;
        }

        public IActionResult CategoriesDropDown()
        {
            var listCategory = _service.GetCategoryDropDown();
            if (listCategory == null) return NotFound();

            return View(listCategory.Data);
        }

        [Route("Category/{category}")]
        public IActionResult GetPostByCategory(string category)
        {
            var listPostByCategory = _postService.GetDeailPostByCategory(category);
            if (listPostByCategory != null)
            {
                return View("~/Views/Post/Index.cshtml", listPostByCategory.Data);
            }
            return View();
        }
    }
}