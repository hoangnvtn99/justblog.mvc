﻿using FA.JustBlog.Service.PostService;
using Microsoft.AspNetCore.Mvc;

namespace FA.JustBlog.Controllers
{
    public class PostController : Controller
    {
        private readonly IPostService _postService;

        public PostController(IPostService postService)
        {
            _postService = postService;
        }

        public IActionResult Index()
        {
            var listPost = _postService.GetAllPost();
            if (listPost != null)
            {
                return View(listPost.Data);
            }
            ViewBag.Message = listPost.Message;
            return View();
        }

        public IActionResult LatestPost()
        {
            var listLasterPost = _postService.GetLastestPost(5);
            if (listLasterPost != null)
            {
                return PartialView("_ListPosts", listLasterPost.Data);
            }
            return PartialView();
        }
        public IActionResult DetailPost(int year, int month, string title)
        {
            var postViewModel = _postService.GetDetailPost(year, month, title);
            if (postViewModel != null)
            {
                return View(postViewModel.Data);
            }
            return View();
        }

        public IActionResult MostViewedPosts()
        {
            var listmost = _postService.GetMostPost(5);
            if (listmost != null)
            {
                return PartialView("_ListPosts", listmost.Data);
            }
            return PartialView();
        }

        [Route("Post/{urlSlug}")]
        public IActionResult GetPostByTag(string urlSlug)
        {
            if (string.IsNullOrEmpty(urlSlug))
            {
                ViewBag.Message = "Not Found";
            }
            var listPost = _postService.GetPostByTag(urlSlug);
            if (listPost != null)
            {
                return View("~/Views/Post/Index.cshtml", listPost.Data);
            }
            return View();
        }
    }
}