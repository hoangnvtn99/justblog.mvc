﻿using FA.JustBlog.Service.PostService;
using FA.JustBlog.Service.TagService;
using Microsoft.AspNetCore.Mvc;

namespace FA.JustBlog.Controllers
{
    public class TagController : Controller
    {
        private readonly ITagService _service;
        private readonly PostService _postService;

        // GET: TagController
        public TagController(ITagService service, PostService postService)
        {
            _service = service;
            _postService = postService;
        }

        public ActionResult PopularTags()
        {
            var listTag = _service.GetPopularTag();
            if (listTag != null)
            {
                return PartialView(listTag.Data);
            }
            return PartialView();
        }

        public ActionResult GetPostByTag(string url)
        {
            var listPost = _postService.GetPostByTag(url);
            if (listPost != null)
            {
                return View("~/Views/Post/Index.cshtml", listPost);
            }
            return View();
        }
    }
}