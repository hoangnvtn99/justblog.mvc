﻿using FA.JustBlog.Model.CommentModel;
using FA.JustBlog.Service.CommentService;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using X.PagedList;

namespace FA.JustBlog.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class CommentController : Controller
    {
        private readonly ICommentService _service;

        public CommentController(ICommentService service)
        {
            _service = service;
        }

        // GET: CommentController
        [Authorize(Policy = "View")]
        public ActionResult Index(int? page, int? pageSize)
        {
            var PageNuber = page ?? 1;
            var PageSize = pageSize ?? 3;
            ViewBag.Comments = _service.GetAllComments().Data.ToPagedList(PageNuber, PageSize);
            ViewBag.page = page;
            ViewBag.pageSize = pageSize;
            return View();
        }

        // GET: CommentController/Details/5
        [Authorize(Policy = "View")]
        public ActionResult Details(int id)
        {
            var comment = _service.GetDetailComment(id).Data;
            if (comment != null)
            {
                return View(comment);
            }
            return Redirect("Error.cshtml");
        }

        // GET: CommentController/Create
        [Authorize(Policy = "Create and Update")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: CommentController/Create
        [Authorize(Policy = "Create and Update")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CommentDetailViewModel comment)
        {
            if (ModelState.IsValid)
            {
                var reponse = _service.CreateComment(comment, comment.PostId);
                TempData["Message"] = reponse.Message;
                TempData["DataError"] = reponse.DataError;

                return RedirectToAction(actionName: "Index", controllerName: "Comment", new { area = "Admin", page = 1, pageSize = 3 });
            }
            return View(comment);
        }

        // GET: CommentController/Edit/5
        [Authorize(Policy = "Create and Update")]
        public ActionResult Edit(int id)
        {
            if (id == 0 || id == null)
            {
                return NotFound();
            }
            var comment = _service.GetDetailComment(id);
            if (comment == null)
            {
                return NotFound();
            }
            return View(comment.Data);
        }

        // POST: CommentController/Edit/5
        [Authorize(Policy = "Create and Update")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(CommentDetailViewModel comment)
        {
            if (ModelState.IsValid)
            {
                var reponse = _service.UpdateComment(comment);
                TempData["Message"] = reponse.Message;
                TempData["DataError"] = reponse.DataError;
                return RedirectToAction(actionName: "Index", controllerName: "Comment", new { area = "Admin", page = 1, pageSize = 3 });
            }
            return View(comment);
        }

        // GET: CommentController/Delete/5
        [Authorize(Policy = "Delete")]
        public ActionResult Delete(int id)
        {
            if (id == 0 || id == null)
            {
                return NotFound();
            }
            var comment = _service.GetDetailComment(id);
            if (comment == null)
            {
                return NotFound();
            }
            return View(comment.Data);
        }

        // POST: CommentController/Delete/5
        [Authorize(Policy = "Delete")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(CommentDetailViewModel comment)
        {
            if (comment == null)
            {
                return NotFound();
            }
            var reponse = _service.DeleteComment(comment);
            if (reponse != null)
            {
                TempData["Message"] = reponse.Message;
                TempData["DataError"] = reponse.DataError;
                return RedirectToAction(actionName: "Index", controllerName: "Comment", new { area = "Admin", page = 1, pageSize = 3 });
            }
            return View(comment);
        }
    }
}