﻿using FA.JustBlog.Entities.Entities;
using FA.JustBlog.Model.UserModel;
using FA.JustBlog.Service.UserService;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace FA.JustBlog.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class LoginController : Controller
    {
        private readonly UserManager<AppUser> _manager;
        private readonly SignInManager<AppUser> _signInManager;
        private readonly IUserService _userService;

        public LoginController(UserManager<AppUser> manager, SignInManager<AppUser> signInManager, IUserService userService)
        {
            _manager = manager;
            _signInManager = signInManager;
            _userService = userService;
        }

        [AllowAnonymous]
        public IActionResult Index()
        {
            return View();
        }

        [AllowAnonymous]
        public async Task<IActionResult> Logout()
        {
            await _signInManager.SignOutAsync();
            return RedirectToAction(actionName: "Index", controllerName: "Login");
        }

        [AllowAnonymous]
        public async Task<IActionResult> SignIn()
        {
            if (HttpContext.User.Identity.IsAuthenticated)
            {
                return RedirectToAction("/Admin");
            }
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> SignIn(UserLoginViewModel userLogin)
        {
            if (ModelState.IsValid && userLogin.RememberMe)
            {
                var userName = _userService.GetUserNameByEmail(userLogin.Email).Data;

                if (userName == null)
                {
                    return NotFound();
                }
                else
                {
                    var result = await _signInManager.PasswordSignInAsync(userName.UserName, userLogin.Password, false, false);

                    if (result.Succeeded)
                    {
                        TempData["Message"] = "Login Success";
                        return Redirect("/Admin");
                    }
                    else
                    {
                        TempData["DataError"] = "Login Failer";
                    }

                    ModelState.AddModelError(string.Empty, "Invalid Login Attempt");
                }
            }
            return View("Index", userLogin);
        }
    }
}