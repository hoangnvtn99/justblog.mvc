﻿using FA.JustBlog.Model.CategoryModel;
using FA.JustBlog.Service.CategoryService;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using X.PagedList;

namespace FA.JustBlog.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class CategoryController : Controller
    {
        private readonly ICategoryService _service;

        public CategoryController(ICategoryService service)
        {
            _service = service;
        }

        [Authorize(Policy = "View")]
        public ActionResult Index(int? page, int? pageSize)
        {
            var PageNuber = page ?? 1;
            var PageSize = pageSize ?? 3;
            ViewBag.Categorys = _service.GetCategoryDropDown().Data.ToPagedList(PageNuber, PageSize);
            ViewBag.page = page;
            ViewBag.pageSize = pageSize;
            return View();
        }

        [Authorize(Policy = "View")]
        public ActionResult Details(int id)
        {
            var category = _service.GetDetailCategory(id).Data;
            if (category != null)
            {
                return View(category);
            }
            return Redirect("Error.cshtml");
        }

        [Authorize(Policy = "Create and Update")]
        // GET: CategoryController/Create
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [Authorize(Policy = "Create and Update")]
        // POST: CategoryController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CategoryDetailViewModel category)
        {
            if (ModelState.IsValid)
            {
                var reponse = _service.AddCategory(category);

                if (reponse != null)
                {
                    TempData["Message"] = reponse.Message;
                    return RedirectToAction(actionName: "Index", controllerName: "Category", new { area = "Admin", page = 1, pageSize = 3 });
                }
                else
                {
                    TempData["DataError"] = reponse.DataError;
                }
            }
            return View(category);
        }

        [Authorize(Policy = "Create and Update")]
        public ActionResult Edit(int id)
        {
            if (id == 0 || id == null)
            {
                return NotFound();
            }
            var category = _service.GetDetailCategory(id);
            if (category == null)
            {
                return NotFound();
            }
            return View(category.Data);
        }

        // POST: CategoryController/Edit/5
        [Authorize(Policy = "Create and Update")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(CategoryDetailViewModel category)
        {
            if (ModelState.IsValid)
            {
                var reponse = _service.UpdateCategory(category);
                if (reponse != null)
                {
                    TempData["Message"] = reponse.Message;
                    return RedirectToAction(actionName: "Index", controllerName: "Category", new { area = "Admin", page = 1, pageSize = 3 });
                }
                else
                {
                    TempData["DataError"] = reponse.DataError;
                }
            }
            return View(category);
        }

        // GET: CategoryController/Delete/5
        [Authorize(Policy = "Delete")]
        public ActionResult Delete(int id)
        {
            if (id == 0 || id == null)
            {
                return NotFound();
            }
            var category = _service.GetDetailCategory(id);
            if (category == null)
            {
                return NotFound();
            }
            return View(category.Data);
        }

        // POST: CategoryController/Delete/5
        [Authorize(Policy = "Delete")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(CategoryDetailViewModel category)
        {
            if (category == null)
            {
                return NotFound();
            }
            var resonse = _service.DeleteCategory(category);

            if (resonse != null)
            {
                TempData["Message"] = resonse.Message;
                return RedirectToAction(actionName: "Index", controllerName: "Category", new { area = "Admin", page = 1, pageSize = 3 });
            }
            else
            {
                TempData["DataError"] = resonse.DataError;
            }
            return View(category);
        }
    }
}