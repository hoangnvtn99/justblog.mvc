﻿using FA.JustBlog.Service.CategoryService;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace FA.JustBlog.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class HomeController : Controller
    {
        private readonly ICategoryService _service;

        public HomeController(ICategoryService service)
        {
            _service = service;
        }

        [Authorize(Policy = "View")]
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Error404()
        {
            return View();
        }

        public IActionResult Contact()
        {
            return View();
        }
    }
}