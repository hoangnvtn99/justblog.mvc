﻿using FA.JustBlog.Model.TagModel;
using FA.JustBlog.Service.TagService;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using X.PagedList;

namespace FA.JustBlog.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class TagController : Controller
    {
        private readonly ITagService _service;

        public TagController(ITagService service)
        {
            _service = service;
        }

        // GET: TagController
        [Authorize(Policy = "View")]
        public ActionResult Index(int? page, int? pageSize)
        {
            var PageNuber = page ?? 1;
            var PageSize = pageSize ?? 3;
            ViewBag.Tags = _service.GetAllTags().Data.ToPagedList(PageNuber, PageSize);
            ViewBag.page = page;
            ViewBag.pageSize = pageSize;
            return View();
        }

        // GET: TagController/Details/5
        [Authorize(Policy = "View")]
        public ActionResult Details(int id)
        {
            var tag = _service.GetDetailTag(id).Data;
            if (tag != null)
            {
                return View(tag);
            }
            return Redirect("Error.cshtml");
        }

        // GET: TagController/Create
        [Authorize(Policy = "Create and Update")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: TagController/Create
        [Authorize(Policy = "Create and Update")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(TagDetailViewModel tag)
        {
            if (ModelState.IsValid)
            {
                var response = _service.AddTag(tag);
                TempData["Message"] = response.Message;
                TempData["DataError"] = response.DataError;
                return RedirectToAction(actionName: "Index", controllerName: "Tag", new { area = "Admin", page = 1, pageSize = 3 });
            }
            return View(tag);
        }

        // GET: TagController/Edit/5
        [Authorize(Policy = "Create and Update")]
        public ActionResult Edit(int id)
        {
            if (id == 0 || id == null)
            {
                return NotFound();
            }
            var reponse = _service.GetDetailTag(id);
            if (reponse == null)
            {
                return NotFound();
            }
            return View(reponse.Data);
        }

        // POST: TagController/Edit/5
        [Authorize(Policy = "Create and Update")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(TagDetailViewModel tagDetail)
        {
            if (ModelState.IsValid)
            {
                var resonse = _service.UpdateTag(tagDetail);
                TempData["Message"] = resonse.Message;
                TempData["DataError"] = resonse.DataError;
                return RedirectToAction(actionName: "Index", controllerName: "Tag", new { area = "Admin", page = 1, pageSize = 3 });
            }
            return View(tagDetail);
        }

        // GET: TagController/Delete/5
        [Authorize(Policy = "Delete")]
        public ActionResult Delete(int id)
        {
            if (id == 0 || id == null)
            {
                return NotFound();
            }
            var reponse = _service.GetDetailTag(id);
            if (reponse == null)
            {
                return NotFound();
            }
            return View(reponse.Data);
        }

        // POST: TagController/Delete/5
        [Authorize(Policy = "Delete")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(TagDetailViewModel tagDetail)
        {
            if (tagDetail == null)
            {
                return NotFound();
            }
            var resonse = _service.DeleteTag(tagDetail);
            if (resonse != null)
            {
                TempData["Message"] = resonse.Message;
                TempData["DataError"] = resonse.DataError;
                return RedirectToAction(actionName: "Index", controllerName: "Tag", new { area = "Admin", page = 1, pageSize = 3 });
            }
            return View(tagDetail);
        }
    }
}