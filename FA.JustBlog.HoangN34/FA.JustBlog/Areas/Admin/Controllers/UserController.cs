﻿using FA.JustBlog.Model.UserModel;
using FA.JustBlog.Service.UserService;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using X.PagedList;

namespace FA.JustBlog.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class UserController : Controller
    {
        private readonly IUserService _service;

        public UserController(IUserService service)
        {
            _service = service;
        }

        [Authorize(Policy = "View")]
        public IActionResult Index(int? page, int? pageSize)
        {
            var pageIndex = page ?? 1;
            var PageSize = pageSize ?? 3;
            ViewBag.Users = _service.GetAllUser().Data.ToPagedList(pageIndex, PageSize);

            ViewBag.page = page;
            ViewBag.pageSize = pageSize;
            return View();
        }

        [Authorize(Policy = "View")]
        public IActionResult Details(string id)
        {
            var category = _service.GetDetailUser(id).Data;
            if (category != null)
            {
                return View(category);
            }
            return Redirect("Error.cshtml");
        }

        [Authorize(Policy = "Create and Update")]
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [Authorize(Policy = "Create and Update")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CreateUserModelView userModel)
        {
            if (ModelState.IsValid)
            {
                var reponse = _service.AddUser(userModel);
                TempData["Message"] = reponse.Message;
                TempData["DataError"] = reponse.DataError;
                return RedirectToAction(actionName: "Index", controllerName: "User", new { area = "Admin", page = 1, pageSize = 3 });
            }
            return View(userModel);
        }

        [Authorize(Policy = "Create and Update")]
        public ActionResult Edit(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return NotFound();
            }
            var user = _service.GetDetailUser(id);

            if (user == null)
            {
                return NotFound();
            }
            return View(user.Data);
        }

        [Authorize(Policy = "Create and Update")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(CreateUserModelView createUser)
        {
            if (ModelState.IsValid)
            {
                var resonse = await _service.UpdateUser(createUser);
                TempData["Message"] = resonse.Message;
                TempData["DataError"] = resonse.DataError;
                return RedirectToAction(actionName: "Index", controllerName: "User", new { area = "Admin", page = 1, pageSize = 3 });
            }
            return View(createUser);
        }

        [Authorize(Policy = "Delete")]
        public ActionResult Delete(string Id)
        {
            if (Id == null)
            {
                return NotFound();
            }
            var category = _service.GetDetailUser(Id);
            if (category == null)
            {
                return NotFound();
            }
            return View(category.Data);
        }

        // POST: CategoryController/Delete/5
        [Authorize(Policy = "Delete")]
        [HttpPost]
        [ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteUser(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return NotFound();
            }
            var resonse = _service.DeleteUser(id);

            if (resonse != null)
            {
                TempData["Message"] = resonse.Message;
                TempData["DataError"] = resonse.DataError;
                return RedirectToAction(actionName: "Index", controllerName: "User", new { area = "Admin", page = 1, pageSize = 3 });
            }
            return View(id);
        }
    }
}