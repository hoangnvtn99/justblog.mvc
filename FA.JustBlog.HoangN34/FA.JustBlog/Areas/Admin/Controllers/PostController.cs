﻿using FA.JustBlog.Model.PostModel;
using FA.JustBlog.Service.CategoryService;
using FA.JustBlog.Service.PostService;
using FA.JustBlog.Service.TagService;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using X.PagedList;

namespace FA.JustBlog.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class PostController : Controller
    {
        private readonly IPostService _service;
        private readonly ICategoryService _category;
        private readonly ITagService _tagService;

        public PostController(IPostService service, ICategoryService category, ITagService tagService)
        {
            _service = service;
            _category = category;
            _tagService = tagService;
        }

        // GET: PostController
        [Authorize(Policy = "View")]
        public ActionResult Index(int page, int? pageSize)
        {
            var PageSize = pageSize ?? 3;
            ViewBag.Posts = _service.GetAllPost().Data.ToPagedList(page, PageSize);
            ViewBag.page = page;
            ViewBag.pageSize = pageSize;
            return View();
        }

        // GET: PostController/Details/5
        [Authorize(Policy = "View")]
        public ActionResult Details(int id)
        {
            if (id == 0 || id == null)
            {
                return NotFound();
            }
            var post = _service.GetDetailPostByPostId(id);
            if (post == null)
            {
                return NotFound();
            }
            return View(post.Data);
        }

        // GET: PostController/Create
        [Authorize(Policy = "Create and Update")]
        public ActionResult Create()
        {
            var dropDownCategory = _category.GetCategoryDropDown();
            var dropDownTag = _tagService.GetAllTags();

            ViewBag.Categorys = new SelectList(dropDownCategory.Data, "Id", "CategoryName");
            ViewBag.Tags = new SelectList(dropDownTag.Data, "Id", "TagName");
            return View();
        }

        // POST: PostController/Create
        [Authorize(Policy = "Create and Update")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(PostDetailViewModel post)
        {
            if (ModelState.IsValid)
            {
                if (post.TotalRate == 0 || post.RateCount == 0)
                {
                    post.TotalRate = 1;
                    post.RateCount = 1;
                }

                var response = _service.AddPost(post);
                TempData["Message"] = response.Message;
                TempData["DataError"] = response.DataError;
                return RedirectToAction(actionName: "Index", controllerName: "Post", new { area = "Admin", page = 1, pageSize = 3 });
            }
            else
            {
                var dropDownCategory = _category.GetCategoryDropDown();
                var dropDownTag = _tagService.GetAllTags();

                ViewBag.Categorys = new SelectList(dropDownCategory.Data, "Id", "CategoryName");
                ViewBag.Tags = new SelectList(dropDownTag.Data, "Id", "TagName");
            }
            return View(post);
        }

        // GET: PostController/Edit/5
        [Authorize(Policy = "Create and Update")]
        public ActionResult Edit(int id)
        {
            if (id == 0 || id == null)
            {
                return NotFound();
            }
            var dropdownCategory = _category.GetCategoryDropDown();
            var dropDownTag = _tagService.GetAllTags();

            var post = _service.GetDetailPostByPostId(id);

            var tagByPost = _tagService.GetTagByPost(id).Data;
            post.Data.Tags = new List<int>();

            foreach (var item in tagByPost)
            {
                post.Data.Tags.Add(item.Id);
            }

            ViewBag.Categorys = new SelectList(dropdownCategory.Data, "Id", "CategoryName");
            ViewBag.Tags = new SelectList(dropDownTag.Data, "Id", "TagName");

            if (post == null)
            {
                return NotFound();
            }
            return View(post.Data);
        }

        // POST: PostController/Edit/5
        [Authorize(Policy = "Create and Update")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(PostDetailViewModel post)
        {
            if (post == null)
            {
                return NotFound();
            }
            if (ModelState.IsValid)
            {
                var response = _service.UpdatePost(post);
                TempData["Message"] = response.Message;
                TempData["DataError"] = response.DataError;
                return RedirectToAction(actionName: "Index", controllerName: "Post", new { area = "Admin", page = 1, pageSize = 3 });
            }
            return View(post);
        }

        // GET: PostController/Delete/5
        [Authorize(Policy = "Delete")]
        public ActionResult Delete(int id)
        {
            if (id == 0 || id == null)
            {
                return NotFound();
            }
            var dropdownCategory = _category.GetCategoryDropDown();
            ViewBag.Categorys = new SelectList(dropdownCategory.Data, "Id", "CategoryName");
            var post = _service.GetDetailPostByPostId(id);
            if (post == null)
            {
                return NotFound();
            }
            return View(post.Data);
        }

        // POST: PostController/Delete/5
        [Authorize(Policy = "Delete")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeletePost(int id)
        {
            if (id == 0 || id == null)
            {
                return NotFound();
            }
            var reponse = _service.DeletePost(id);
            TempData["Message"] = reponse.Message;
            TempData["DataError"] = reponse.DataError;
            if (reponse != null)
            {
                return RedirectToAction(actionName: "Index", controllerName: "Post", new { area = "Admin", page = 1, pageSize = 3 });
            }
            return View("Delete");
        }
    }
}