﻿using FA.JustBlog.Model.RoleModel;
using FA.JustBlog.Service.RoleService;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using X.PagedList;

namespace FA.JustBlog.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class RoleController : Controller
    {
        private readonly IRoleService _service;

        public RoleController(IRoleService service)
        {
            _service = service;
        }

        // GET: RoleController
        [Authorize(Policy = "View")]
        public ActionResult Index(int? page, int? pageSize)
        {
            var pageIndex = page ?? 1;
            var PageSize = pageSize ?? 3;
            ViewBag.Roles = _service.GetAllRole().Data.ToPagedList(pageIndex, PageSize);

            ViewBag.page = page;
            ViewBag.pageSize = pageSize;
            return View();
        }

        // GET: RoleController/Details/5
        [Authorize(Policy = "View")]
        public ActionResult Details(string id)
        {
            var category = _service.GetDetailRole(id).Data;
            if (category != null)
            {
                return View(category);
            }
            return Redirect("Error.cshtml");
        }

        // GET: RoleController/Create
        [Authorize(Policy = "Create and Update")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: RoleController/Create
        [Authorize(Policy = "Create and Update")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(RoleModel roleModel)
        {
            if (ModelState.IsValid)
            {
                var reponse = _service.AddRole(roleModel);
                TempData["Message"] = reponse.Message;
                TempData["DataError"] = reponse.DataError;
                return RedirectToAction(actionName: "Index", controllerName: "Role", new { area = "Admin", page = 1, pageSize = 3 });
            }
            return View(roleModel);
        }

        // GET: RoleController/Edit/5
        [Authorize(Policy = "Create and Update")]
        public ActionResult Edit(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return NotFound();
            }
            var user = _service.GetDetailRole(id);

            if (user == null)
            {
                return NotFound();
            }
            return View(user.Data);
        }

        // POST: RoleController/Edit/5
        [Authorize(Policy = "Create and Update")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(RoleModel roleModel)
        {
            if (ModelState.IsValid)
            {
                var reponse = _service.UpdateRole(roleModel);
                TempData["Message"] = reponse.Message;
                TempData["DataError"] = reponse.DataError;
                return RedirectToAction(actionName: "Index", controllerName: "Role", new { area = "Admin", page = 1, pageSize = 3 });
            }
            return View(roleModel);
        }

        // GET: RoleController/Delete/5
        [ActionName("Delete")]
        public ActionResult Delete(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return NotFound();
            }
            var user = _service.GetDetailRole(id);

            if (user == null)
            {
                return NotFound();
            }
            return View(user.Data);
        }

        // POST: RoleController/Delete/5
        [ActionName("Delete")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteRole(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return NotFound();
            }
            var reponse = _service.DeleteRole(id);

            if (reponse != null)
            {
                TempData["Message"] = reponse.Message;
                TempData["DataError"] = reponse.DataError;
                return RedirectToAction(actionName: "Index", controllerName: "User", new { area = "Admin", page = 1, pageSize = 3 });
            }
            return View(id);
        }
    }
}