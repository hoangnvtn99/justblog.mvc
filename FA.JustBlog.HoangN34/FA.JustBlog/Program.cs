﻿using FA.JustBlog.Entities.Entities;
using FA.JustBlog.Service.CategoryService;
using FA.JustBlog.Service.CommentService;
using FA.JustBlog.Service.Mapper;
using FA.JustBlog.Service.PostService;
using FA.JustBlog.Service.RoleService;
using FA.JustBlog.Service.TagService;
using FA.JustBlog.Service.UserService;
using JustBlog.Entities.DataContext;
using JustBlog.Repository.Infrastructure;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using NLog;
using NLog.Web;

var logger = NLog.LogManager.Setup().LoadConfigurationFromAppSettings().GetCurrentClassLogger();
logger.Debug("init main");

try
{
    var builder = WebApplication.CreateBuilder(args);

    // Add services to the container.
    builder.Services.AddControllersWithViews();

    // Config Database
    builder.Services.AddDbContext<JustBlogContext>(options =>
    {
        options.UseSqlServer(builder.Configuration.GetConnectionString("DefaultConnection"));
    });

    //Config Mapper
    builder.Services.AddAutoMapper(typeof(AutoMapConfig));

    // Add service
    builder.Services.AddMvc();
    builder.Services.AddScoped<IUnitOfWork, UnitOfWork>();
    builder.Services.AddScoped<IPostService, PostService>();
    builder.Services.AddScoped<ITagService, TagService>();
    builder.Services.AddScoped<ICategoryService, CategoryService>();
    builder.Services.AddScoped<ICommentService, CommentService>();
    builder.Services.AddScoped<IUserService, UserService>();
    builder.Services.AddScoped<IRoleService, RoleService>();

    //builder.Services.AddSingleton<ILogger, Logger>();

    // add service identity
    builder.Services.AddIdentity<AppUser, IdentityRole>(options => options.SignIn.RequireConfirmedAccount = false)
                    .AddEntityFrameworkStores<JustBlogContext>()
                    .AddDefaultTokenProviders();

    //add dependency injection
    builder.Logging.ClearProviders();
    builder.Logging.SetMinimumLevel(Microsoft.Extensions.Logging.LogLevel.Trace);
    builder.Host.UseNLog();

    // Config Session
    builder.Services.AddSession(options =>
    {
        options.IdleTimeout = TimeSpan.FromDays(1);
        options.Cookie.HttpOnly = true;
        options.Cookie.IsEssential = true;
    });

    // Truy cập IdentityOptions
    builder.Services.ConfigureApplicationCookie(options =>
    {
        options.LoginPath = "/Admin/Login/Index";
        options.AccessDeniedPath = new PathString("/Admin/Home/Error404");
    });

    builder.Services.Configure<IdentityOptions>(options =>
    {
        // Thiết lập về Password
        options.Password.RequireDigit = false; // Không bắt phải có số
        options.Password.RequireLowercase = false; // Không bắt phải có chữ thường
        options.Password.RequireNonAlphanumeric = false; // Không bắt ký tự đặc biệt
        options.Password.RequireUppercase = false; // Không bắt buộc chữ in
        options.Password.RequiredLength = 6; // Số ký tự tối thiểu của password
        options.Password.RequiredUniqueChars = 1; // Số ký tự riêng biệt

        // Cấu hình Lockout - khóa user
        options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(5); // Khóa 5 phút
        options.Lockout.MaxFailedAccessAttempts = 5; // Thất bại 5 lầ thì khóa
        options.Lockout.AllowedForNewUsers = true;

        // Cấu hình về User.
        options.User.AllowedUserNameCharacters = // các ký tự đặt tên user
            "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._@+";
        options.User.RequireUniqueEmail = true;  // Email là duy nhất

        // Cấu hình đăng nhập.
        options.SignIn.RequireConfirmedEmail = false;           // Cấu hình xác thực địa chỉ email (email phải tồn tại)
        options.SignIn.RequireConfirmedPhoneNumber = false;     // Xác thực số điện thoại
    });

    builder.Services.AddAuthorization(config =>
    {
        config.AddPolicy("View", policy => policy.RequireRole(new string[] { "User", "Contributor", "Blog owner" }));
        config.AddPolicy("Create and Update", policy => policy.RequireRole(new string[] { "Contributor", "Blog owner" }));
        config.AddPolicy("Delete", policy => policy.RequireRole(new string[] { "Blog owner" }));
    });

    var app = builder.Build();

    // Configure the HTTP request pipeline.
    if (!app.Environment.IsDevelopment())
    {
        app.UseExceptionHandler("/Home/Error");
        // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
        app.UseHsts();
    }
    app.UseHttpsRedirection();

    app.UseStaticFiles();

    app.UseRouting();

    //app.Use((contect, nect) =>
    //{
    //    Console.WriteLine(contect.Request.Path);
    //    return nect.Invoke();
    //});

    app.UseAuthentication();
    app.UseAuthorization();

    app.UseEndpoints(endpoint =>
    {
        endpoint.MapControllerRoute(
        name: "MyArea",
        pattern: "{area:exists}/{controller=Home}/{action=Index}/{id?}");

        //endpoint.MapControllerRoute(
        //            name: "popular tags",
        //            pattern: "tag/partialview/populartags",
        //            defaults: new { controller = "Tag", action = "PopularTags" }
        //        );

        //endpoint.MapControllerRoute(
        //    name: "category dropdown",
        //    pattern: "category/partialview/categoriesDropdown",
        //    defaults: new { controller = "Category", action = "CategoriesDropDown" }
        //);

        endpoint.MapControllerRoute(
            name: "post details",
            pattern: "post/{year}/{month}/{title}",
            defaults: new { controller = "Post", action = "DetailPost" }
        );

        //endpoint.MapControllerRoute(
        //name: "Post",
        //pattern: "{controller=Post}/{year}/{month}/{title}",
        //defaults: new { controller = "Post", action = "Details" }
        //);

        endpoint.MapControllerRoute(
        name: "default",
        pattern: "{controller=Home}/{action=Index}/{id?}"
        );
    });

    app.Run();
}
catch (Exception ex)
{
    // NLog: catch setup errors
    logger.Error(ex.Message, "Stopped program because of exception");
    //throw
}
finally
{
    // Ensure to flush and stop internal timers/threads before application-exit (Avoid segmentation fault on Linux)
    NLog.LogManager.Shutdown();
}