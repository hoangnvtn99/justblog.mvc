﻿using JustBlog.Entities.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace JustBlog.Entities.Config
{
    public class PostConfig : IEntityTypeConfiguration<Post>
    {
        public void Configure(EntityTypeBuilder<Post> builder)
        {
            builder.ToTable("Posts", "dbo");
            builder.HasKey(p => p.Id);
            builder.Property(p => p.Id).ValueGeneratedOnAdd();
            builder.Property(p => p.PostedOn).HasDefaultValue(DateTime.Now);
            builder.Property(p => p.PostedOn).HasColumnName("Posted On");
            builder.Property(p => p.Title).HasMaxLength(100);
            builder.Property(p => p.ShortDescription).HasMaxLength(2000);
            builder.Property(p => p.PostContent).HasMaxLength(2000);
            builder.Property(p => p.UrlSlug).HasMaxLength(100);
        }
    }
}