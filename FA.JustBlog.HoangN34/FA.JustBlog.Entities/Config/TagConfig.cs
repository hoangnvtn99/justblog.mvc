﻿using JustBlog.Entities.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FA.JustBlog.Entities.Config
{
    public class TagConfig : IEntityTypeConfiguration<Tag>
    {
        public void Configure(EntityTypeBuilder<Tag> builder)
        {
            builder.ToTable("Tags", "dbo");
            builder.HasKey(t => t.Id);
            builder.Property(t => t.Id).ValueGeneratedOnAdd();
            builder.Property(t => t.TagName).IsRequired().HasMaxLength(200);
            builder.Property(t => t.UrlSlug).HasMaxLength(255);
            builder.Property(t => t.Description).HasMaxLength(2000).HasColumnName("Description");
        }
    }
}