﻿namespace JustBlog.Entities.Entities
{
    public class Tag
    {
        public int Id { get; set; }

        public string TagName { get; set; } = string.Empty;

        public string UrlSlug { get; set; } = string.Empty;

        public string Description { get; set; } = string.Empty;

        public int Count { get; set; }
        public virtual ICollection<PostTagMap>? PostTagMaps { get; set; }
    }
}