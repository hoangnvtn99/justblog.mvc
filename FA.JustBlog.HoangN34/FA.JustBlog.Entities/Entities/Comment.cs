﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace JustBlog.Entities.Entities
{
    public class Comment
    {
        public int Id { get; set; }
        public string CommentName { get; set; } = string.Empty;

        [EmailAddress]
        public string Email { get; set; } = string.Empty;

        public string CommentHeader { get; set; } = string.Empty;

        public string CommentText { get; set; } = string.Empty;
        public DateTime CommentTime { get; set; }

        [ForeignKey("PostId")]
        public int PostId { get; set; }

        public Post? Post { get; set; }
    }
}