﻿using System.ComponentModel.DataAnnotations.Schema;

namespace JustBlog.Entities.Entities
{
    public class Post
    {
        public int Id { get; set; }

        public string Title { get; set; } = string.Empty;

        public string ShortDescription { get; set; } = string.Empty;

        public string PostContent { get; set; } = string.Empty;

        public string UrlSlug { get; set; } = string.Empty;

        public bool Published { get; set; }

        public DateTime PostedOn { get; set; }

        public DateTime Modified { get; set; }

        public int ViewCount { get; set; }

        public int RateCount { get; set; }

        public int TotalRate { get; set; }

        [NotMapped]
        public decimal Rate { get => (TotalRate / RateCount); }

        [ForeignKey("CategoryId")]
        public int CategoryId { get; set; }

        public Category? Category { get; set; }
        public virtual ICollection<PostTagMap>? PostTagMaps { get; set; }

        public virtual ICollection<Comment>? Comments { get; set; }
    }
}