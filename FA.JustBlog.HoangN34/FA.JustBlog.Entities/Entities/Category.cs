﻿namespace JustBlog.Entities.Entities
{
    public class Category
    {
        public int Id { get; set; }

        public string CategoryName { get; set; } = string.Empty;

        public string UrlSlug { get; set; } = string.Empty;

        public string Description { get; set; } = string.Empty;

        public virtual ICollection<Post>? Posts { get; set; }
    }
}