﻿using Microsoft.AspNetCore.Identity;

namespace FA.JustBlog.Entities.Entities
{
    public class AppUser : IdentityUser
    {
        public int Age { get; set; }
        public string? AboutMe { get; set; }
        public bool RememberMe { get; set; }
    }
}