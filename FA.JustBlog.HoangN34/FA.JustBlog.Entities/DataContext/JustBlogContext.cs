﻿using FA.JustBlog.Entities.Config;
using JustBlog.Entities.Config;
using JustBlog.Entities.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace JustBlog.Entities.DataContext
{
    public class JustBlogContext : IdentityDbContext
    {
        private readonly DbContextOptions _context;

        public JustBlogContext(DbContextOptions<JustBlogContext> context) : base(context)
        {
            _context = context;
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new PostConfig());

            modelBuilder.ApplyConfiguration(new PostTagMapConfig());

            modelBuilder.ApplyConfiguration(new CommentConfig());

            modelBuilder.ApplyConfiguration(new CategoryConfig());

            modelBuilder.ApplyConfiguration(new TagConfig());

            modelBuilder.Entity<PostTagMap>().HasKey(t => new { t.PostId, t.TagId });

            //modelBuilder.Entity<Users>().ToTable("User").HasKey(u => u.Id);

            modelBuilder.Seed();

            base.OnModelCreating(modelBuilder);
        }

        public DbSet<Category> Categories { get; set; }
        public DbSet<Post> Posts { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<PostTagMap> PostTagMaps { get; set; }
        public DbSet<Comment> Comments { get; set; }
    }
}