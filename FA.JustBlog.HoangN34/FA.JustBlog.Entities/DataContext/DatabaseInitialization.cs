﻿using FA.JustBlog.Entities.Entities;
using JustBlog.Entities.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace JustBlog.Entities.DataContext
{
    public static class DatabaseInitialization
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Category>().HasData(
                new Category()
                {
                    Id = 1,
                    CategoryName = "Niki Kavoura",
                    UrlSlug = "UrlSlug1",
                    Description = "The paper aims to examine the development of competitive advantage in Facebook for brand awareness. The paper describes a dynamic model of the simulation system associated with the stock of online communication and customer engagement, the increase of users and the geographic connection that social media provide."
                },
                new Category()
                {
                    Id = 2,
                    CategoryName = "Nasiopoulos",
                    UrlSlug = "UrlSlug2",
                    Description = "The paper aims to examine the development of competitive advantage in Facebook for brand awareness. The paper describes a dynamic model of the simulation system associated with the stock of online communication and customer engagement, the increase of users and the geographic connection that social media provide."
                },
                new Category()
                {
                    Id = 3,
                    CategoryName = "Damianos P. Sakas",
                    UrlSlug = "UrlSlug3",
                    Description = "The paper aims to examine the development of competitive advantage in Facebook for brand awareness. The paper describes a dynamic model of the simulation system associated with the stock of online communication and customer engagement, the increase of users and the geographic connection that social media provide."
                },
                new Category()
                {
                    Id = 4,
                    CategoryName = "Category4",
                    UrlSlug = "UrlSlug4",
                    Description = "The paper aims to examine the development of competitive advantage in Facebook for brand awareness. The paper describes a dynamic model of the simulation system associated with the stock of online communication and customer engagement, the increase of users and the geographic connection that social media provide."
                },
                new Category()
                {
                    Id = 5,
                    CategoryName = "Category5",
                    UrlSlug = "UrlSlug5",
                    Description = "The paper aims to examine the development of competitive advantage in Facebook for brand awareness. The paper describes a dynamic model of the simulation system associated with the stock of online communication and customer engagement, the increase of users and the geographic connection that social media provide."
                },
                new Category()
                {
                    Id = 6,
                    CategoryName = "Category6",
                    UrlSlug = "UrlSlug6",
                    Description = "The paper aims to examine the development of competitive advantage in Facebook for brand awareness. The paper describes a dynamic model of the simulation system associated with the stock of online communication and customer engagement, the increase of users and the geographic connection that social media provide."
                },
                 new Category()
                 {
                     Id = 7,
                     CategoryName = "Category7",
                     UrlSlug = "UrlSlug7",
                     Description = "The paper aims to examine the development of competitive advantage in Facebook for brand awareness. The paper describes a dynamic model of the simulation system associated with the stock of online communication and customer engagement, the increase of users and the geographic connection that social media provide."
                 },
                  new Category()
                  {
                      Id = 8,
                      CategoryName = "Category8",
                      UrlSlug = "UrlSlug7",
                      Description = "The paper aims to examine the development of competitive advantage in Facebook for brand awareness. The paper describes a dynamic model of the simulation system associated with the stock of online communication and customer engagement, the increase of users and the geographic connection that social media provide."
                  }, new Category()
                  {
                      Id = 9,
                      CategoryName = "Category9",
                      UrlSlug = "UrlSlug9",
                      Description = "The paper aims to examine the development of competitive advantage in Facebook for brand awareness. The paper describes a dynamic model of the simulation system associated with the stock of online communication and customer engagement, the increase of users and the geographic connection that social media provide."
                  },
                   new Category()
                   {
                       Id = 10,
                       CategoryName = "Category10",
                       UrlSlug = "UrlSlug10",
                       Description = "The paper aims to examine the development of competitive advantage in Facebook for brand awareness. The paper describes a dynamic model of the simulation system associated with the stock of online communication and customer engagement, the increase of users and the geographic connection that social media provide."
                   },
                    new Category()
                    {
                        Id = 11,
                        CategoryName = "Category11",
                        UrlSlug = "UrlSlug11",
                        Description = "The paper aims to examine the development of competitive advantage in Facebook for brand awareness. The paper describes a dynamic model of the simulation system associated with the stock of online communication and customer engagement, the increase of users and the geographic connection that social media provide."
                    }

            );
            modelBuilder.Entity<Post>().HasData(
                 new Post()
                 {
                     Id = 1,
                     Title = "What is Lorem Ipsum?",
                     ShortDescription = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard",
                     PostContent = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
                     UrlSlug = "UrlSlug1",
                     Published = true,
                     PostedOn = DateTime.Now,
                     Modified = DateTime.Parse("2022-10-27"),
                     TotalRate = 10,
                     ViewCount = 100,
                     RateCount = 10,
                     CategoryId = 1
                 },
                 new Post()
                 {
                     Id = 2,
                     Title = "Why do we use it?",
                     ShortDescription = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard",
                     PostContent = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
                     UrlSlug = "UrlSlug2",
                     Published = true,
                     PostedOn = DateTime.Now,
                     Modified = DateTime.Parse("2022-10-27"),
                     TotalRate = 10,
                     ViewCount = 100,
                     RateCount = 10,
                     CategoryId = 2
                 },
                 new Post()
                 {
                     Id = 3,
                     Title = "Where does it come from?",
                     ShortDescription = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard",
                     PostContent = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
                     UrlSlug = "UrlSlug3",
                     Published = true,
                     PostedOn = DateTime.Now,
                     Modified = DateTime.Parse("2022-10-27"),
                     TotalRate = 10,
                     ViewCount = 100,
                     RateCount = 10,
                     CategoryId = 3
                 },
                 new Post()
                 {
                     Id = 4,
                     Title = "Where does it come from?",
                     ShortDescription = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard",
                     PostContent = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
                     UrlSlug = "UrlSlug4",
                     Published = true,
                     PostedOn = DateTime.Now,
                     Modified = DateTime.Parse("2022-10-27"),
                     TotalRate = 10,
                     ViewCount = 100,
                     RateCount = 10,
                     CategoryId = 3
                 },
                 new Post()
                 {
                     Id = 5,
                     Title = "Where does it come from?",
                     ShortDescription = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard",
                     PostContent = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
                     UrlSlug = "UrlSlug5",
                     Published = true,
                     PostedOn = DateTime.Now,
                     Modified = DateTime.Parse("2022-10-27"),
                     TotalRate = 10,
                     ViewCount = 100,
                     RateCount = 10,
                     CategoryId = 1
                 },
                 new Post()
                 {
                     Id = 6,
                     Title = "Where does it come from?",
                     ShortDescription = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard",
                     PostContent = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
                     UrlSlug = "UrlSlug6",
                     Published = true,
                     PostedOn = DateTime.Now,
                     Modified = DateTime.Parse("2022-10-27"),
                     TotalRate = 10,
                     ViewCount = 100,
                     RateCount = 10,
                     CategoryId = 2
                 },
                  new Post()
                  {
                      Id = 7,
                      Title = "Where does it come from?",
                      ShortDescription = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard",
                      PostContent = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
                      UrlSlug = "UrlSlug6",
                      Published = true,
                      PostedOn = DateTime.Now,
                      Modified = DateTime.Parse("2022-10-27"),
                      TotalRate = 10,
                      ViewCount = 100,
                      RateCount = 10,
                      CategoryId = 3
                  },
                   new Post()
                   {
                       Id = 8,
                       Title = "Where does it come from?",
                       ShortDescription = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard",
                       PostContent = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
                       UrlSlug = "UrlSlug6",
                       Published = true,
                       PostedOn = DateTime.Now,
                       Modified = DateTime.Parse("2022-10-27"),
                       TotalRate = 10,
                       ViewCount = 100,
                       RateCount = 10,
                       CategoryId = 2
                   },
                    new Post()
                    {
                        Id = 9,
                        Title = "Where does it come from?",
                        ShortDescription = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard",
                        PostContent = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
                        UrlSlug = "UrlSlug6",
                        Published = true,
                        PostedOn = DateTime.Now,
                        Modified = DateTime.Parse("2022-10-27"),
                        TotalRate = 10,
                        ViewCount = 100,
                        RateCount = 10,
                        CategoryId = 4
                    },
                     new Post()
                     {
                         Id = 10,
                         Title = "Where does it come from?",
                         ShortDescription = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard",
                         PostContent = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
                         UrlSlug = "UrlSlug6",
                         Published = true,
                         PostedOn = DateTime.Now,
                         Modified = DateTime.Parse("2022-10-27"),
                         TotalRate = 10,
                         ViewCount = 100,
                         RateCount = 10,
                         CategoryId = 5
                     },
                      new Post()
                      {
                          Id = 11,
                          Title = "Where does it come from?",
                          ShortDescription = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard",
                          PostContent = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
                          UrlSlug = "UrlSlug6",
                          Published = true,
                          PostedOn = DateTime.Now,
                          Modified = DateTime.Parse("2022-10-27"),
                          TotalRate = 10,
                          ViewCount = 100,
                          RateCount = 10,
                          CategoryId = 6
                      }
                );
            modelBuilder.Entity<Tag>().HasData(
                new Tag()
                {
                    Id = 1,
                    TagName = "Economy",
                    UrlSlug = "UrlSlug1",
                    Description = "It is a long established The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).",
                    Count = 1
                },
                new Tag()
                {
                    Id = 2,
                    TagName = "LifeStyle",
                    UrlSlug = "UrlSlug2",
                    Description = "It is a long established fact that a reader. as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).",
                    Count = 2
                },
                new Tag()
                {
                    Id = 3,
                    TagName = "Society",
                    UrlSlug = "UrlSlug3",
                    Description = "It is a  content of a page when looking at its layout. Ipsum is that it has a more-or-less sometimes on purpose (injected humour and the like).",
                    Count = 3
                },
                new Tag()
                {
                    Id = 4,
                    TagName = "Culture",
                    UrlSlug = "UrlSlug4",
                    Description = "It is a  content of a page when looking at its layout. Ipsum is that it has a more-or-less sometimes on purpose (injected humour and the like).",
                    Count = 3
                },
                new Tag()
                {
                    Id = 5,
                    TagName = "Politics",
                    UrlSlug = "UrlSlug4",
                    Description = "It is a  content of a page when looking at its layout. Ipsum is that it has a more-or-less sometimes on purpose (injected humour and the like).",
                    Count = 3
                },
                new Tag()
                {
                    Id = 6,
                    TagName = "Art",
                    UrlSlug = "UrlSlug4",
                    Description = "It is a  content of a page when looking at its layout. Ipsum is that it has a more-or-less sometimes on purpose (injected humour and the like).",
                    Count = 10
                },
                new Tag()
                {
                    Id = 11,
                    TagName = "NathanSpoor",
                    UrlSlug = "UrlSlug11",
                    Description = "It is a  content of a page when looking at its layout. Ipsum is that it has a more-or-less sometimes on purpose (injected humour and the like).",
                    Count = 33
                },
                new Tag()
                {
                    Id = 7,
                    TagName = "Introduction",
                    UrlSlug = "UrlSlug4",
                    Description = "It is a  content of a page when looking at its layout. Ipsum is that it has a more-or-less sometimes on purpose (injected humour and the like).",
                    Count = 3
                },
                new Tag()
                {
                    Id = 8,
                    TagName = "Medicilous",
                    UrlSlug = "UrlSlug4",
                    Description = "It is a  content of a page when looking at its layout. Ipsum is that it has a more-or-less sometimes on purpose (injected humour and the like).",
                    Count = 3
                },
                new Tag()
                {
                    Id = 9,
                    TagName = "Nicolefallek",
                    UrlSlug = "UrlSlug4",
                    Description = "It is a  content of a page when looking at its layout. Ipsum is that it has a more-or-less sometimes on purpose (injected humour and the like).",
                    Count = 3
                },
                new Tag()
                {
                    Id = 10,
                    TagName = "ChuckRobinSon",
                    UrlSlug = "UrlSlug4",
                    Description = "It is a  content of a page when looking at its layout. Ipsum is that it has a more-or-less sometimes on purpose (injected humour and the like).",
                    Count = 30
                },
                new Tag()
                {
                    Id = 12,
                    TagName = "SamuelTefcon",
                    UrlSlug = "UrlSlug12",
                    Description = "It is a  content of a page when looking at its layout. Ipsum is that it has a more-or-less sometimes on purpose (injected humour and the like).",
                    Count = 40
                }
            );
            modelBuilder.Entity<PostTagMap>().HasData(
                new PostTagMap()
                {
                    PostId = 1,
                    TagId = 1,
                },
                new PostTagMap()
                {
                    PostId = 1,
                    TagId = 2,
                },
                new PostTagMap()
                {
                    PostId = 1,
                    TagId = 3,
                },
                new PostTagMap()
                {
                    PostId = 2,
                    TagId = 1,
                }, new PostTagMap()
                {
                    PostId = 2,
                    TagId = 3,
                },
                new PostTagMap()
                {
                    PostId = 3,
                    TagId = 1,
                },
                new PostTagMap()
                {
                    PostId = 3,
                    TagId = 2,
                },
                new PostTagMap()
                {
                    PostId = 3,
                    TagId = 3,
                },
                new PostTagMap()
                {
                    PostId = 4,
                    TagId = 1,
                },
                new PostTagMap()
                {
                    PostId = 4,
                    TagId = 2,
                },
                new PostTagMap()
                {
                    PostId = 4,
                    TagId = 3,
                }

                );
            modelBuilder.Entity<Comment>().HasData(
                new Comment()
                {
                    Id = 1,
                    CommentName = "But that was then, right?",
                    Email = "hoangnv34@gmail.com",
                    CommentHeader = "Well, they tend to suck.",
                    CommentText = "Blog comments are a relic from a bygone era.",
                    PostId = 1,
                },
                new Comment()
                {
                    Id = 2,
                    CommentName = "Instead, let people see the real you.",
                    Email = "hoangnv25@gmail.com",
                    CommentHeader = "Blog comments are a relic from a bygone era.",
                    CommentText = "That’s the word on the street, isn’t it?",
                    PostId = 2,
                },
                new Comment()
                {
                    Id = 3,
                    CommentName = "The same is true in blog commenting.",
                    Email = "hoangnv10@gmail.com",
                    CommentHeader = "But that was then, right?",
                    CommentText = "Today, readers have turned to Facebook, Instagram, and YouTube for their commenting fix. Spammers and trolls have taken over. And, as a result, some blog owners have done what would’ve once been unthinkable: turned off their comments.",
                    PostId = 3,
                },
                new Comment()
                {
                    Id = 4,
                    CommentName = "The same is true in blog commenting.",
                    Email = "hoangnv10@gmail.com",
                    CommentHeader = "But that was then, right?",
                    CommentText = "Today, readers have turned to Facebook, Instagram, and YouTube for their commenting fix. Spammers and trolls have taken over. And, as a result, some blog owners have done what would’ve once been unthinkable: turned off their comments.",
                    PostId = 3,
                },
                  new Comment()
                  {
                      Id = 5,
                      CommentName = "The same is true in blog commenting.",
                      Email = "hoangnv10@gmail.com",
                      CommentHeader = "But that was then, right?",
                      CommentText = "Today, readers have turned to Facebook, Instagram, and YouTube for their commenting fix. Spammers and trolls have taken over. And, as a result, some blog owners have done what would’ve once been unthinkable: turned off their comments.",
                      PostId = 2,
                  },
                  new Comment()
                  {
                      Id = 6,
                      CommentName = "The same is true in blog commenting.",
                      Email = "hoangnv10@gmail.com",
                      CommentHeader = "But that was then, right?",
                      CommentText = "Today, readers have turned to Facebook, Instagram, and YouTube for their commenting fix. Spammers and trolls have taken over. And, as a result, some blog owners have done what would’ve once been unthinkable: turned off their comments.",
                      PostId = 3,
                  },
                  new Comment()
                  {
                      Id = 7,
                      CommentName = "The same is true in blog commenting.",
                      Email = "hoangnv10@gmail.com",
                      CommentHeader = "But that was then, right?",
                      CommentText = "Today, readers have turned to Facebook, Instagram, and YouTube for their commenting fix. Spammers and trolls have taken over. And, as a result, some blog owners have done what would’ve once been unthinkable: turned off their comments.",
                      PostId = 3,
                  },
                  new Comment()
                  {
                      Id = 8,
                      CommentName = "The same is true in blog commenting.",
                      Email = "hoangnv10@gmail.com",
                      CommentHeader = "But that was then, right?",
                      CommentText = "Today, readers have turned to Facebook, Instagram, and YouTube for their commenting fix. Spammers and trolls have taken over. And, as a result, some blog owners have done what would’ve once been unthinkable: turned off their comments.",
                      PostId = 3,
                  },
                  new Comment()
                  {
                      Id = 9,
                      CommentName = "The same is true in blog commenting.",
                      Email = "hoangnv10@gmail.com",
                      CommentHeader = "But that was then, right?",
                      CommentText = "Today, readers have turned to Facebook, Instagram, and YouTube for their commenting fix. Spammers and trolls have taken over. And, as a result, some blog owners have done what would’ve once been unthinkable: turned off their comments.",
                      PostId = 1,
                  },
                    new Comment()
                    {
                        Id = 10,
                        CommentName = "The same is true in blog commenting.",
                        Email = "hoangnv10@gmail.com",
                        CommentHeader = "But that was then, right?",
                        CommentText = "Today, readers have turned to Facebook, Instagram, and YouTube for their commenting fix. Spammers and trolls have taken over. And, as a result, some blog owners have done what would’ve once been unthinkable: turned off their comments.",
                        PostId = 1,
                    },
                     new Comment()
                     {
                         Id = 11,
                         CommentName = "The same is true in blog commenting.",
                         Email = "hoangnv10@gmail.com",
                         CommentHeader = "But that was then, right?",
                         CommentText = "Today, readers have turned to Facebook, Instagram, and YouTube for their commenting fix. Spammers and trolls have taken over. And, as a result, some blog owners have done what would’ve once been unthinkable: turned off their comments.",
                         PostId = 1,
                     }

                );

            var userRole = new IdentityRole[]
            {
                new IdentityRole()
                {
                    Id = Guid.NewGuid().ToString(),
                    Name = "User",
                    NormalizedName = "USER"
                },
                new IdentityRole()
                {
                    Id = Guid.NewGuid().ToString(),
                    Name = "Contributor",
                    NormalizedName = "CONTRIBUTOR"
                },
                new IdentityRole()
                {
                    Id = Guid.NewGuid().ToString(),
                    Name = "Blog owner",
                    NormalizedName = "BLOG OWNER"
                }
            };

            modelBuilder.Entity<IdentityRole>().HasData(
               userRole
            );
            var listUser = new AppUser[]
            {
                new AppUser()
                {
                    Id = Guid.NewGuid().ToString(),
                    UserName = "hoangnv34",
                    PasswordHash = "abasdfsdfddd",
                    NormalizedUserName = "hoangnv34".ToUpper(),
                    Email = "hoangnv34@gmail.com",
                    NormalizedEmail = "hoangnv34@gmail.com".ToUpper(),
                    EmailConfirmed = true,
                    AccessFailedCount = 0,
                    Age = 18,
                    AboutMe = "Lorem ipsum dolor sit amet, consectetur adipiscing elit"
                },
                new AppUser()
                {
                    Id = Guid.NewGuid().ToString(),
                    UserName = "hoangnv35",
                    PasswordHash = "123456",
                    NormalizedUserName = "hoangnv35".ToUpper(),
                    Email = "hoangnv35@gmail.com",
                    NormalizedEmail = "hoangnv35@gmail.com".ToUpper(),
                    EmailConfirmed = true,
                    AccessFailedCount = 0,
                    Age = 22,
                    AboutMe = "Lorem ipsum dolor sit amet, consectetur adipiscing elit"
                },
                new AppUser()
                {
                    Id = Guid.NewGuid().ToString(),
                    UserName = "admin",
                    PasswordHash = "admin123",
                    NormalizedUserName = "admin".ToUpper(),
                    Email = "hoangnv24@gmail.com",
                    NormalizedEmail = "hoangnv24@gmail.com".ToUpper(),
                    EmailConfirmed = true,
                    AccessFailedCount = 0,
                    Age = 25,
                    AboutMe = "Lorem ipsum dolor sit amet, consectetur adipiscing elit"
                }
            };

            var passwordHash = new PasswordHasher<AppUser>();

            foreach (var user in listUser)
            {
                user.PasswordHash = passwordHash.HashPassword(user, "hoang1234");
            }

            modelBuilder.Entity<AppUser>().HasData(
                listUser
            );

            modelBuilder.Entity<IdentityUserRole<string>>().HasData(
                new IdentityUserRole<string>
                {
                    RoleId = userRole[0].Id,
                    UserId = listUser[0].Id
                },
                new IdentityUserRole<string>
                {
                    RoleId = userRole[1].Id,
                    UserId = listUser[1].Id
                },
                new IdentityUserRole<string>
                {
                    RoleId = userRole[2].Id,
                    UserId = listUser[2].Id
                }
            );
        }
    }
}