﻿using JustBlog.Repository.Infrastructure;
using Microsoft.AspNetCore.Identity;

namespace FA.JustBlog.Repository.RoleRepo
{
    public interface IRoleRepository : IRepositoryBase<IdentityRole>
    {
        /// <summary>
        ///  Delete role by idRole
        /// </summary>
        /// <param name="idRole"></param>
        public void DeleleRoleById(string idRole);
    }
}