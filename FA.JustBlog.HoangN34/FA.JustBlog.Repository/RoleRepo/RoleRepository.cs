﻿using JustBlog.Entities.DataContext;
using JustBlog.Repository.Infrastructure;
using Microsoft.AspNetCore.Identity;

namespace FA.JustBlog.Repository.RoleRepo
{
    public class RoleRepository : RepositoryBase<IdentityRole>, IRoleRepository
    {
        public RoleRepository(JustBlogContext context) : base(context)
        {
        }

        public void DeleleRoleById(string idRole)
        {
            _context.Roles.Remove(_context.Roles.Find(idRole));
        }
    }
}