﻿using FA.JustBlog.Repository.RoleRepo;
using FA.JustBlog.Repository.UserRepo;
using JustBlog.Entities.DataContext;
using JustBlog.Repository.CategoryRepo;
using JustBlog.Repository.Comment;
using JustBlog.Repository.CommentRepo;
using JustBlog.Repository.PostRepo;
using JustBlog.Repository.TagRepo;

namespace JustBlog.Repository.Infrastructure
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly JustBlogContext _context;
        private ICategoryRepository _categoryRepository;
        private IPostRepository _postRepository;
        private ITagRepository _tagRepository;
        private ICommentRepository _commentRepository;
        private IUserRepository _userRepository;
        private IRoleRepository _roleRepository;

        public ICategoryRepository categoryRepository => _categoryRepository ?? new CategoryRepository(_context);

        public IPostRepository postRepository => _postRepository ?? new PostRepository(_context);

        public ITagRepository tagRepository => _tagRepository ?? new TagRepository(_context);

        public ICommentRepository commentRepository => _commentRepository ?? new CommentRepository(_context);

        public IUserRepository userRepository => _userRepository ?? new UserReponsitory(_context);
        public IRoleRepository roleRepository => _roleRepository ?? new RoleRepository(_context);

        public UnitOfWork(JustBlogContext context)
        {
            _context = context;
        }

        public void Dispose()
        {
            _context.Dispose();
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposing)
            {
                return;
            }
            this._context?.Dispose();
        }

        public int SaveChanges()
        {
            return _context.SaveChanges();
        }
    }
}