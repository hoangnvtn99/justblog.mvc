﻿namespace JustBlog.Repository.Infrastructure
{
    public interface IRepositoryBase<TEntity> where TEntity : class
    {
        /// <summary>
        /// Change state of entity to added
        /// </summary>
        /// <param name="entity"></param>
        public void Create(TEntity entity);

        /// <summary>
        ///  Change state of entities to added
        /// </summary>
        /// <param name="entities"></param>
        public void CreateRange(List<TEntity> entities);

        /// <summary>
        /// Change state of entity to deleted
        /// </summary>
        /// <param name="entity"></param>
        public void Delete(TEntity entity);

        /// <summary>
        /// Delete <paramref name="TEntity"></paramref> from database
        /// </summary>
        /// <param name="ids"></param>
        /// <returns>void</returns>
        public void Delete(int primaryKey);

        /// <summary>
        /// Change state of entity to modified
        /// </summary>
        /// <param name="entity"></param>
        public void Update(TEntity entity);

        /// <summary>
        /// Change state of entities to modified
        /// </summary>
        /// <param name="entity"></param>
        public void UpdateRange(List<TEntity> entities);

        /// <summary>
        /// Get all <paramref name="TEntity"></paramref> from database by Id
        /// </summary>
        /// <returns>Return List TEntity</returns>
        public IEnumerable<TEntity> GetAll();

        /// <summary>
        /// Get <paramref name="TEntity"></paramref> from database
        /// </summary>
        /// <param name="ids"></param>
        /// <returns>Return TEntity if found else return other</returns>
        public TEntity Find(int primaryKey);

        /// <summary>
        /// Get List <paramref name="TEntity"></paramref> from database by criterias
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns>Return List IEnumerable if found else null</returns>
        public IEnumerable<TEntity> Find(Func<TEntity, bool> predicate);

        /// <summary>
        ///    Get TEntity from database
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns>Return TEntity if found else return null</returns>
        public TEntity FindByValue(Func<TEntity, bool> predicate);
    }
}