﻿using FA.JustBlog.Repository.RoleRepo;
using FA.JustBlog.Repository.UserRepo;
using JustBlog.Repository.CategoryRepo;
using JustBlog.Repository.Comment;
using JustBlog.Repository.PostRepo;
using JustBlog.Repository.TagRepo;

namespace JustBlog.Repository.Infrastructure
{
    public interface IUnitOfWork : IDisposable
    {
        public ICategoryRepository categoryRepository { get; }
        public IPostRepository postRepository { get; }
        public ITagRepository tagRepository { get; }
        public ICommentRepository commentRepository { get; }

        public IUserRepository userRepository { get; }
        public IRoleRepository roleRepository { get; }

        int SaveChanges();
    }
}