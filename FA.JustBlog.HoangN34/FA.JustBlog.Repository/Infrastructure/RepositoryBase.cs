﻿using JustBlog.Entities.DataContext;
using Microsoft.EntityFrameworkCore;

namespace JustBlog.Repository.Infrastructure
{
    public class RepositoryBase<TEntity> : IRepositoryBase<TEntity> where TEntity : class
    {
        protected readonly JustBlogContext _context;
        protected DbSet<TEntity> _dbSet;
        private readonly UnitOfWork _unitOfWork;

        public RepositoryBase(JustBlogContext context)
        {
            _context = context;
            _dbSet = context.Set<TEntity>();
            _unitOfWork = new UnitOfWork(_context);
        }

        public void Create(TEntity entity)
        {
            _context.Add(entity);
        }

        public void CreateRange(List<TEntity> entities)
        {
            _dbSet.AddRange(entities);
        }

        public void Delete(TEntity entity)
        {
            _dbSet.Remove(entity);
        }

        public void Delete(int primaryKey)
        {
            _dbSet.Remove(_dbSet.Find(primaryKey));
        }

        public TEntity Find(int primaryKey)
        {
            return _dbSet.Find(primaryKey);
        }

        public IEnumerable<TEntity> Find(Func<TEntity, bool> predicate)
        {
            return _dbSet.Where(predicate).AsQueryable().AsNoTracking();
        }

        public TEntity FindByValue(Func<TEntity, bool> predicate)
        {
            return _dbSet.FirstOrDefault(predicate);
        }

        public IEnumerable<TEntity> GetAll()
        {
            return _dbSet.ToList();
        }

        public void Update(TEntity entity)
        {
            _context.Update(entity);
        }

        public void UpdateRange(List<TEntity> entities)
        {
            _context.UpdateRange(entities);
        }
    }
}