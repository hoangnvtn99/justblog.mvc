﻿using JustBlog.Entities.Entities;
using JustBlog.Repository.Infrastructure;

namespace JustBlog.Repository.TagRepo
{
    public interface ITagRepository : IRepositoryBase<Tag>
    {
        /// <summary>
        /// Get Tag by urlSlug
        /// </summary>
        /// <param name="urlSlug"></param>
        /// <returns>Return Tag if found it else return null</returns>
        Tag GetTagByUrlSlug(string urlSlug);

        /// <summary>
        /// Get popular tags
        /// </summary>
        /// <returns>Return a list tags</returns>
        IEnumerable<Tag> GetPopularTags(int size);

        /// <summary>
        ///  Get tag by postId
        /// </summary>
        /// <param name="postId"></param>
        /// <returns>Return list tag if found</returns>
        public IEnumerable<Tag> GetTagByPost(int postId);
    }
}