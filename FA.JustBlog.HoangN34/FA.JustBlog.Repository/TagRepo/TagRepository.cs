﻿using JustBlog.Entities.DataContext;
using JustBlog.Entities.Entities;
using JustBlog.Repository.Infrastructure;
using Microsoft.EntityFrameworkCore;

namespace JustBlog.Repository.TagRepo
{
    public class TagRepository : RepositoryBase<Tag>, ITagRepository
    {
        private readonly JustBlogContext _context;

        public TagRepository(JustBlogContext context) : base(context)
        {
            _context = context;
        }

        public IEnumerable<Tag> GetPopularTags(int size)
        {
            return _context.Tags.OrderByDescending(t => t.Count).Take(size).ToList();
        }

        public Tag GetTagByUrlSlug(string urlSlug)
        {
            return _context.Tags.FirstOrDefault(t => t.UrlSlug == urlSlug);
        }

        public IEnumerable<Tag> GetTagByPost(int postId)
        {
            return _context.Posts.Where(s => s.Id == postId).Include(pm => pm.PostTagMaps).ThenInclude(p => p.Tag).SelectMany(t => t.PostTagMaps.Select(x => x.Tag)).ToList();
        }
    }
}