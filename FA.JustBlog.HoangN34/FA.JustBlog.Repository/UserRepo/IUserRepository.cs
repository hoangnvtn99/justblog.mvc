﻿using FA.JustBlog.Entities.Entities;
using JustBlog.Repository.Infrastructure;

namespace FA.JustBlog.Repository.UserRepo
{
    public interface IUserRepository : IRepositoryBase<AppUser>
    {
        /// <summary>
        ///  Delete user by id
        /// </summary>
        /// <param name="Id"></param>
        public void DeleteUserById(string Id);
    }
}