﻿using FA.JustBlog.Entities.Entities;
using JustBlog.Entities.DataContext;
using JustBlog.Repository.Infrastructure;

namespace FA.JustBlog.Repository.UserRepo
{
    public class UserReponsitory : RepositoryBase<AppUser>, IUserRepository
    {
        public UserReponsitory(JustBlogContext context) : base(context)
        {
        }

        public void DeleteUserById(string Id)
        {
            _context.Users.Remove(_context.Users.Find(Id));
        }
    }
}