﻿using JustBlog.Entities.DataContext;
using JustBlog.Entities.Entities;
using JustBlog.Repository.Infrastructure;
using Microsoft.EntityFrameworkCore;

namespace JustBlog.Repository.PostRepo
{
    public class PostRepository : RepositoryBase<Post>, IPostRepository
    {
        private readonly JustBlogContext _context;

        public PostRepository(JustBlogContext context) : base(context)
        {
            _context = context;
        }

        public int CountPostsForCategory(string category)
        {
            return _context.Categories.Where(c => c.CategoryName.Contains(category, StringComparison.OrdinalIgnoreCase)).Include(c => c.Posts).Count();
        }

        public int CountPostsForTag(string tag)
        {
            return _context.Tags.Where(t => t.TagName.Contains(tag, StringComparison.OrdinalIgnoreCase)).Include(pt => pt.PostTagMaps).SelectMany(p => p.PostTagMaps.Select(p => p.PostId)).Count();
        }

        public IEnumerable<Post> GetHighestPosts(int size)
        {
            return _context.Posts.AsEnumerable().OrderByDescending(s => s.Rate).Take(size).ToList();
        }

        public IEnumerable<Post> GetLatestPost(int size)
        {
            return _context.Posts.OrderByDescending(p => p.PostedOn).Take(size).ToList();
        }

        public IEnumerable<Post> GetMostViewedPost(int size)
        {
            return _context.Posts.OrderByDescending(s => s.ViewCount).Take(size).ToList();
        }

        public IEnumerable<Post> GetPostsByCategory(string category)
        {
            return _context.Categories.Where(c => c.UrlSlug == category).Include(p => p.Posts).SelectMany(c => c.Posts).ToList();
        }

        public IEnumerable<Post> GetPostsByMonth(DateTime monthYear)
        {
            return _context.Posts.Where(p => p.PostedOn == monthYear).ToList();
        }

        public IEnumerable<Post> GetPostsByTag(string tag)
        {
            return _context.Tags.Where(t => t.UrlSlug == tag).Include(pt => pt.PostTagMaps).ThenInclude(ptm => ptm.Post)
                .SelectMany(t => t.PostTagMaps.Select(p => p.Post)).ToList();
        }

        public IEnumerable<Post> GetPublisedPosts()
        {
            return _context.Posts.Where(p => p.Published).ToList();
        }

        public IEnumerable<Post> GetUnpublisedPosts()
        {
            return _context.Posts.Where(p => p.Published == false).ToList();
        }

        public void CreatePostTagMap(int postId, List<int> tags)
        {
            foreach (var tag in tags)
            {
                PostTagMap postTagMap = new PostTagMap() { PostId = postId, TagId = tag };
                _context.PostTagMaps.Add(postTagMap);
            }
        }

        public void DeletePostTagMap(int postId)
        {
            var post = _context.PostTagMaps.Where(p => p.PostId == postId).ToList();
            _context.PostTagMaps.RemoveRange(post);
        }
    }
}