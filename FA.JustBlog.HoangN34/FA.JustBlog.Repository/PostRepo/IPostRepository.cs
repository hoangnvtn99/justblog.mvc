﻿using JustBlog.Entities.Entities;
using JustBlog.Repository.Infrastructure;

namespace JustBlog.Repository.PostRepo
{
    public interface IPostRepository : IRepositoryBase<Post>
    {
        /// <summary>
        /// Get publised post
        /// </summary>
        /// <returns>Return list post</returns>
        public IEnumerable<Post> GetPublisedPosts();

        /// <summary>
        ///  Get list post
        /// </summary>
        /// <returns>Return list post</returns>
        public IEnumerable<Post> GetUnpublisedPosts();

        /// <summary>
        ///  Get Latest Post by take size
        /// </summary>
        /// <param name="size"></param>
        /// <returns>Return list post</returns>
        public IEnumerable<Post> GetLatestPost(int size);

        /// <summary>
        ///  Get post by month
        /// </summary>
        /// <param name="monthYear"></param>
        /// <returns>Return list post</returns>
        public IEnumerable<Post> GetPostsByMonth(DateTime monthYear);

        /// <summary>
        ///  Count post by category
        /// </summary>
        /// <param name="category"></param>
        /// <returns>Return count of post</returns>
        public int CountPostsForCategory(string category);

        /// <summary>
        ///  Get post by category
        /// </summary>
        /// <param name="category"></param>
        /// <returns>Return list post</returns>
        public IEnumerable<Post> GetPostsByCategory(string category);

        /// <summary>
        ///  Count post by categoryName
        /// </summary>
        /// <param name="tag"></param>
        /// <returns>Return count of tag</returns>
        public int CountPostsForTag(string tag);

        /// <summary>
        /// Get post by tagName
        /// </summary>
        /// <param name="tag"></param>
        /// <returns></returns>
        public IEnumerable<Post> GetPostsByTag(string tag);

        /// <summary>
        ///  Get most view post by take size
        /// </summary>
        /// <param name="size"></param>
        /// <returns>Return list post</returns>
        IEnumerable<Post> GetMostViewedPost(int size);

        /// <summary>
        ///  Get highest post by take size
        /// </summary>
        /// <param name="size"></param>
        /// <returns>Return a list post</returns>
        IEnumerable<Post> GetHighestPosts(int size);

        /// <summary>
        ///  Add post by tags
        /// </summary>
        /// <param name="post"></param>
        /// <param name="tags"></param>
        public void CreatePostTagMap(int postId, List<int> tags);

        /// <summary>
        ///  Delete postTagMap by postId
        /// </summary>
        /// <param name="postId"></param>
        public void DeletePostTagMap(int postId);
    }
}