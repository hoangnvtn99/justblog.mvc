﻿using JustBlog.Entities.DataContext;
using JustBlog.Entities.Entities;
using JustBlog.Repository.Infrastructure;
using Microsoft.EntityFrameworkCore;

namespace JustBlog.Repository.CategoryRepo
{
    public class CategoryRepository : RepositoryBase<Category>, ICategoryRepository
    {
        private readonly JustBlogContext _context;

        public CategoryRepository(JustBlogContext context) : base(context)
        {
            _context = context;
        }

        public Category GetCategoryByPost(int postId)
        {
            return _context.Posts.Where(p => p.Id == postId).Include(s => s.Category).Select(x => x.Category).FirstOrDefault();
        }
    }
}