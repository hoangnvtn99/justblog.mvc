﻿using JustBlog.Entities.Entities;
using JustBlog.Repository.Infrastructure;

namespace JustBlog.Repository.CategoryRepo
{
    public interface ICategoryRepository : IRepositoryBase<Category>
    {
        /// <summary>
        ///  Get Category by post
        /// </summary>
        /// <param name="postId"></param>
        /// <returns>Return a category</returns>
        public Category GetCategoryByPost(int postId);
    }
}