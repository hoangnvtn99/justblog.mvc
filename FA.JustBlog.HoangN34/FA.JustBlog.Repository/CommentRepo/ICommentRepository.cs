﻿using JustBlog.Entities.Entities;
using JustBlog.Repository.Infrastructure;

namespace JustBlog.Repository.Comment
{
    public interface ICommentRepository : IRepositoryBase<JustBlog.Entities.Entities.Comment>
    {
        /// <summary>
        ///   Add comment to database
        /// </summary>
        /// <param name="postId"></param>
        /// <param name="commentName"></param>
        /// <param name="commentEmail"></param>
        /// <param name="commentTitle"></param>
        /// <param name="commentBody"></param>
        void AddComment(int postId, string commentName, string commentEmail, string commentTitle, string commentBody);

        /// <summary>
        /// Get comment by postid
        /// </summary>
        /// <param name="postId"></param>
        /// <returns>Return IEnumerable comment</returns>
        IEnumerable<JustBlog.Entities.Entities.Comment> GetCommentsForPost(int postId);

        /// <summary>
        ///  Get comment by post
        /// </summary>
        /// <param name="post"></param>
        /// <returns>Return IEnumerable comment</returns>
        IEnumerable<JustBlog.Entities.Entities.Comment> GetCommentsForPost(Post post);
    }
}