﻿using JustBlog.Entities.DataContext;
using JustBlog.Entities.Entities;
using JustBlog.Repository.Comment;
using JustBlog.Repository.Infrastructure;
using Microsoft.EntityFrameworkCore;

namespace JustBlog.Repository.CommentRepo
{
    public class CommentRepository : RepositoryBase<JustBlog.Entities.Entities.Comment>, ICommentRepository
    {
        private readonly JustBlogContext _context;

        public CommentRepository(JustBlogContext context) : base(context)
        {
            _context = context;
        }

        public void AddComment(int postId, string commentName, string commentEmail, string commentTitle, string commentBody)
        {
            var comment = new Entities.Entities.Comment()
            {
                PostId = postId,
                CommentName = commentName,
                Email = commentEmail,
                CommentHeader = commentTitle,
                CommentText = commentBody,
                CommentTime = DateTime.Now,
            };
            _context.Add(comment);
        }

        public IEnumerable<Entities.Entities.Comment> GetCommentsForPost(int postId)
        {
            return _context.Posts.Where(p => p.Id == postId).Include(c => c.Comments).SelectMany(s => s.Comments).OrderByDescending(s => s.CommentTime).ToList();
        }

        public IEnumerable<Entities.Entities.Comment> GetCommentsForPost(Post post)
        {
            return _context.Posts.Where(p => p == post).Include(c => c.Comments).SelectMany(s => s.Comments).OrderByDescending(s => s.CommentTime).ToList();
        }
    }
}